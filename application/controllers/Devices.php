<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Devices extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct(){
		parent::__construct();
		$this->load->helper('date');
		$this->table="devices";
	}
	
	public function index()
	{
			if($this->fb_rest->isloggedin()){
			$data = array();
			$page_no = $this->uri->segment('2');
			$per_page = $this->input->get_post("no_items", true);
			$search = $this->input->get_post("search", true);
			$sort_fld = $this->input->get_post("sort_fld", true);
			$sort_dir = $this->input->get_post("sort_dir", true);
			$page_burl = site_url("/devices");
			$this->table="devices";
			$table_name = $this->table;
			$params =  array("page_no" => $page_no, "per_page" => $per_page, "uri_segment" => "2",
			"search" => $search, "sort_fld" => $sort_fld, "sort_dir" => $sort_dir, "page_burl" => $page_burl, "table_name" => $table_name);
			
			$data["sort_fld"] = $sort_fld;
			$data["sort_dir"] = $sort_dir;
			$data["search"] = $search;
			$data["per_page"] = $per_page;
			
			$sort_columns = array("device_id", "device_name", "device_status");
			//echo fb_text("feed_stock_distribution_list"); 
			$hstr = array("device_id" =>"device_id", "device_name" => "device_name", "device_status" =>"device_status");
			
			$theader = "";
			
			foreach($hstr as $hk => $hv)
			{
				if(in_array($hk, $sort_columns)){
					$cdir = ($hk == $sort_fld) ? (($sort_dir=="asc") ? "desc" : "asc" ) : "asc";
					$pstr = (!empty($per_page)) ? $per_page : "10";
					$srt_params = array("sort_fld" => $hk, "sort_dir" => $cdir, "no_items" => $pstr, "search" => $search);
					$srt_str = http_build_query($srt_params);
					$srt_url = site_url("/devices?$srt_str");
					$cdir_icon = "";
					if(!empty($sort_fld)){
						$cdir_icon = ($hk == $sort_fld) ? 
						(($sort_dir=="asc") ? "&nbsp;<i class=\"fa fa-sort-asc\"></i>" : "&nbsp;<i class=\"fa fa-sort-desc\"></i>" ) : "";
					}
					$thstr = $hv.$cdir_icon;
					$thtml = "<th><a href='$srt_url'>$thstr</a></th>";
					$theader .= $thtml."\n";
				}else{
					$theader .= "<th>$hv</th>\n";
				}
			}
			
			$data["theader"] = $theader;
			$msg  = $this->fb_rest->list_record($params);
		

			
            $this->load->view('include/header');
            $this->load->view('include/left-sidebar');
			if($msg["status"] == "success")
			{
				$data["page_links"] = $msg["page_links"];
				$data["result_set"] = $msg["result_set"];
				$this->load->view("devices", $data);
			}else{
				$this->load->view("layout/error", $data);
			}
			$this->load->view('include/footer');
			
		}else{
			redirect('/login');
		}
	}
	public function add_record(){
		$form_data = $this->input->post();
		
		$device_id = $this->input->post("device_id");
		$device_name = $this->input->post("device_name");
		$device_status = $this->input->post("device_status");
		$watts = $this->input->post("watts");
		$device_type = $this->input->post("device_type");



			$table_name = "devices";
			$idata = array("device_id" => $device_id, 
			"device_status" => $device_status, 
			"device_name" => $device_name,
			"amps" => 0,
			"voltage" => 0,
			"hrs" => 0,
			"watts" => $watts,
			"device_type" => $device_type,
			"createdtime" => now(), 
			"updatedtime" => now());
			
			$result = $this->fb_rest->create_record($table_name, $idata);
			 fb_pr($result);
		if($result['status']=="success"){
			$this->session->set_flashdata('success',fb_text("success"));
			redirect('/devices');
		}else{
			$this->session->set_flashdata('failed',fb_text("failed"));
			redirect('/devices');
		}
	}
	public function list_record(){
		$table_name = "devices";
		$this->fb_rest->test_list_record($table_name);
	}
	
	public function updatedevice_status(){
		$device_id = $this->input->post("device_id");
		
		$table_name = "devices";
		$from = 0;
		$size = 100;
		$orderfld = "updatedtime";
		$orderdir = "desc";
		$this->load->library('parser');
		$this->parser->set_delimiters("__","__");
		$qpms = array("size" => $size, "from" => $from, "orderfld" => $orderfld, "orderdir" => $orderdir, "device_id" => $device_id);
		$query_str = $this->parser->parse('query/update_state', $qpms, true);
		$result = $this->fb_rest->get_query_result($table_name, $query_str);
		//print_r($result);
		//echo json_encode($result,true);
	     if($result["status"] == "success"){
			$rst = $result["result_set"];
			foreach($rst as $row){
				$idata = $row["_source"];
				if($idata["device_status"] == "true"){
				$idata["device_status"] = "false";
				}
				elseif($idata["device_status"] == "false"){
				$idata["device_status"] = "true";
				}
				//fb_pr($idata);
				//fb_pr($row["_id"]); 
				$this->fb_rest->update_record($table_name, $idata, $row["_id"]);
                echo json_encode($idata);
			}
		} 
		
   }
    

 public function details($id){
     $ci =& get_instance();
	$table_name="devices";
	$aresult = $ci->fb_rest->search_list($table_name, $id);
	//print_r($aresult); exit;
	$cresult = isset($aresult["result_set"]) ? $aresult["result_set"]: array();
	if($aresult["status"] == "success" && !empty($cresult[0])){
		
		$crow = $cresult[0];
		$csrc = $crow["_source"];
		 if(!empty($cresult) & !empty($csrc["device_name"])){
            $data["result_set"] = $aresult["result_set"];
             $data["device"] = $csrc;
			$this->load->view("device_details", $data);
		 }else{
			redirect('/devices');
		}
        $this->load->view('include/footer');
        
	}
     
 }
    
public function deviceinGenset($rid){
     $ci =& get_instance();
        $table_name="genset";
		$genset = $ci->fb_rest->get_genset($table_name, $rid);
        //$genset = $ci->fb_rest->get_record($table_name, $rid);
        if($genset['status']=="success" && $genset['result_set']['_source']['device_id']){
            return true;
        }else{
            return false;
        }
}
    
 
}
