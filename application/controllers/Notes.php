<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notes extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->helper('date');
		$this->session->keep_flashdata('update_success');
		$this->session->keep_flashdata('update_failed');				
	}
	
	public function index()
	{
		if($this->fb_rest->isloggedin()){
			if(1) {
				$data=array();
				$presult_data = array();
				$rec_id = fb_fetch_id("notes"); // "350435";
				$result = $this->iot_rest->getnotes_list($rec_id);
				//print_r($result); exit();
				$this->load->view('include/header_view');
				$this->load->view('include/left-sidebar');
				if($result["status"] == "success"){
					
					
					$data["presult_data"] = $result["data"];
					$this->load->view('notes',$data);	
				}
				else{
					$this->load->view("layout/error", $data);
				}
				$this->load->view('include/footer');
		    } else {
				$this->load->view('alert/permission');
			}

		}else{
			redirect('/login');
		}
	}

		public function add_notes(){
		
		 $table_name = "notes";
    	 $action = $this->input->post("action");
     	 $rid = $this->input->post("rid");
		 $notes_title = $this->input->post("notes_title");
		 $notes_description = $this->input->post("notes_description");
		 $notes_date = $this->input->post("notes_date");
		 
     	 if($action=="update" && $rid){
			$notes_date = strtotime($this->input->post('notes_date')) *1000;
     	 	$form_data = $this->input->post();
     	 	$form_data['updatedtime']=now();
     	 	$form_data['notes_title']=$notes_title;
     	 	$form_data['notes_description'] = $notes_description;
     	 	$form_data['date'] = $notes_date;
     	 	//print_r($form_data);  exit();
     	 	$result = $this->fb_rest->update_record($table_name,$form_data,$rid);
     	 }else{
			$notes_date = strtotime($this->input->post('notes_date')) *1000;
			$useremail = $this->session->userdata("email");
			$idata = array("notes_title" => $notes_title, 
			"notes_description" => $notes_description,
			"date" => $notes_date,
			"status" => "true",
			"email" => $useremail,
			"send_status" => "not send",
			"createdtime" => time(), 
			"updatedtime" => time());
			//print_r($idata); exit();
			$result = $this->fb_rest->create_record($table_name, $idata);     	 	
     	 }

			if($result['status']=="success"){
				$this->session->set_flashdata('notes_success','notes/remainder added/updated successfully');
				redirect('/notes');
			}else{
				$this->session->set_flashdata('notes_failed','please try again later');
				redirect('/notes');
			}

	}	
	
	public function updateStatus($rid,$status){
		$table_name="notes";
		$form_data =  array();
		if ($status=="false") {
			$form_data['status']="false";	
		}else{
			$form_data['status']="true";	
		}
		//print_r($form_data); exit();
		$rkey = $rid;
		$result = $this->fb_rest->update_record($table_name,$form_data,$rkey);
		//print_r($result); exit();
		if($result['status']=="success"){
			if($status=="false")
				$this->session->set_flashdata('update_success',"Notes has been de-activated successfully");
			else
				$this->session->set_flashdata('update_success',"Notes has been activated successfully");
			redirect('/notes');
		}else{
			$this->session->set_flashdata('update_failed',"notes status update failed");
			redirect('/notes');
		}
	}
	
	public function delete(){
		$table_name="notes";
		$rkey = $this->input->post("rid");	
		$result= $this->fb_rest->delete_record($table_name, $rkey);
		
		if($result['status']=="success"){
			$this->session->set_flashdata('delete_success',"notes deleted");
			redirect('/notes');
		}else{
			$this->session->set_flashdata('delete_failed',"issue deleting notes");
			redirect('/notes');
		}
	}


	
	
		

	

			
}
?>