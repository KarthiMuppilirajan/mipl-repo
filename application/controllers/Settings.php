<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	
	
	public function __construct() {
		parent::__construct();
		$this->load->helper('date');
		$this->table="alert_settings";
		
	}

	public function index()
	{
		/*if($this->fb_rest->isloggedin()){
			$this->load->view('settings_content');
		}	*/		
		
		  if($this->fb_rest->isloggedin()){
			if($this->fb_rest->has_accessable("settings")) {
				$table_name=$this->table;
				$data = array();
				$table_name = "alert_settings_dg";
				$params =  array("page_no" => 1, "per_page" =>1, "uri_segment" => "2",
					"search" => "", "sort_fld" => "updatedtime", "sort_dir" => "desc", "page_burl" => "", "table_name" => $table_name);
				$msg2  = $this->fb_rest->getlist_record($params);
				
				$table_name = "alert_settings";
				$params =  array("page_no" => 1, "per_page" =>1, "uri_segment" => "2",
					"search" => "", "sort_fld" => "updatedtime", "sort_dir" => "desc", "page_burl" => "", "table_name" => $table_name);
				$msg  = $this->fb_rest->getlist_record($params);
				
				//fb_pr($msg);
				$table_name1 = "alert_notify";
				$params1 =  array("page_no" => 1, "per_page" =>10, "uri_segment" => "2",
					"search" => "", "sort_fld" => "createdtime", "sort_dir" => "desc", "page_burl" => "", "table_name" => $table_name1);
				$msg1 = $this->fb_rest->getlist_record($params1);

				$table_name = "alert_status";
				$params =  array("page_no" => 1, "per_page" =>10, "uri_segment" => "2",
					"search" => "", "sort_fld" => "updatedtime", "sort_dir" => "desc", "page_burl" => "", "table_name" => $table_name);
				$msg3  = $this->fb_rest->getlist_record($params);
				$alert_status_result_set = $msg3["result_set"];
				
				if($msg["status"] == "success" && $msg1['status']== 'success' && $msg2['status']== 'success')
				{
					$data["result_set"] = $msg["result_set"];
					$data["result_set1"] = $msg1["result_set"];
					$data["dgresult_set"] = $msg2["result_set"];
					$data["alert_status_result_set"] = $alert_status_result_set;
					//fb_pr($data);
					$fuleData=$this->fb_rest->getfuelSettings();

					$data['fuel_set'] = $fuleData;
					//fb_pr($data); exit();
					$this->load->view('settings_content', $data);
				}
				else{
					$this->load->view("layout/error", $data);
				}
			} else {
				$this->load->view('alert/permission');
			}
		} else{
			redirect('/login');
		} 
	}
	
	public function create(){
		$table_name="alert_settings";
		//$tbl_id= 350401;
		fb_clear_cache(array("*alert_settings*"));
		$form_data = $this->input->post();
		$form_data['createdtime']=now();
		$form_data['updatedtime']=now();
		$rkey = $this->input->post("ebalert_id");	
		//print_r($form_data); exit();
		$meter_id = $this->input->post('meter_id');
		if($rkey){
			$result = $this->fb_rest->update_record($table_name,$form_data,$rkey);
		}else{
		//print_r($form_data); exit();
			$result = $this->fb_rest->create_record($table_name,$form_data);
		}
//		print_r($result);
		if($result['status']=="success"){
		$this->session->set_flashdata('c_success',"Success, EB Settings are updated");
			redirect('settings/eb_settings/'.$meter_id);
		}else{
		$this->session->set_flashdata('c_failed',"Oops,something went wrong,Please try again");
			redirect('settings/eb_settings/'.$meter_id);
		}
	}
    
	public function create_genset(){
		$table_name="alert_settings_dg";
		//$tbl_id= 350401;
		fb_clear_cache(array("*alert_settings_dg*"));
		$form_data = $this->input->post();
		$form_data['createdtime']=now();
		$form_data['updatedtime']=now();
		$rkey = $this->input->post("dgalert_id");	
		$meter_id = $this->input->post('meter_id');
		$dg_id = $this->input->post('dg_id');
		if($rkey){
			$result = $this->fb_rest->update_record($table_name,$form_data,$rkey);
		}else{
		//print_r($form_data); exit();
			$result = $this->fb_rest->create_record($table_name,$form_data);
		}
//		print_r($result);
		if($result['status']=="success"){
		$this->session->set_flashdata('c_dg_success',"Success, DG Settings are updated");
			redirect('/settings/dg_settings/'.$meter_id.'/'.$dg_id);
		}else{
		$this->session->set_flashdata('c_dg_failed',"Oops,something went wrong,Please try again");
			redirect('/settings/dg_settings/'.$meter_id.'/'.$dg_id);
		}
	}
	
	public function add_record(){
		$table_name="alert_notify";
		//$tbl_id= 350401;
		fb_clear_cache(array("*alert_notify*"));
		$form_data = $this->input->post();
		$form_data['createdtime']=now();
		$form_data['updatedtime']=now();
		$result = $this->fb_rest->create_record($table_name,$form_data);
//		print_r($result);
		if($result['status']=="success"){
			$this->session->set_flashdata('a_success',"Success, notification settings added successfully");
			redirect('/settings');
		}else{
			$this->session->set_flashdata('a_failed',"Oops!,something went wrong,Please try again");
			redirect('/settings');
		}
	}

	public function updateAlertflag($rid,$status){
		$table_name="alert_status";
		fb_clear_cache(array("*alert_status*"));
		$form_data =  array();
		if ($status=="false") {
			$form_data['alert_flag']="false";	
		}else{
			$form_data['alert_flag']="true";	
		}
		//print_r($form_data); exit();
		$rkey = $rid;
		$result = $this->fb_rest->update_record($table_name,$form_data,$rkey);
		//print_r($result); exit();
		if($result['status']=="success"){
			///read json
			$fname = FCPATH.'json/notification.json';
			$jsonString = file_get_contents($fname);
			$data = json_decode($jsonString, true);
			//fb_pr($data);
			//exit;
			if($status=="false")
			{
				$data['email_alert'] = "false";
				$this->session->set_flashdata('alert_status_update_success',"Respective alert has been de-activated successfully");
			}
			else
			{
				$data['email_alert'] = "true";
				$this->session->set_flashdata('alert_status_update_success',"Respective alert has been activated successfully");
			}
			////write json
			$newJsonString = json_encode($data);
			file_put_contents($fname, $newJsonString);
			//fb_pr($data);
			//exit;
			redirect('/settings');
		}else{
			$this->session->set_flashdata('alert_status_update_failed',"alert status update failed");
			redirect('/settings');
		}
	}
	
	
	
	function delete(){
		// echo "welcome";
		$table_name="alert_notify";
		fb_clear_cache(array("*alert_notify*"));
		$rkey = $this->input->post("rid");	
		$result= $this->fb_rest->delete_record($table_name, $rkey);
		
		if($result['status']=="success"){
			$this->session->set_flashdata('delete_success',"Success, deleted notification settings");
			redirect('/settings');
		}else{
			$this->session->set_flashdata('delete_failed',"Oops,something went wrong,Please try again");
			redirect('/settings');
		}
	}
	
	function edit(){
		$data = array();
		$table_name="alert_notify";
		fb_clear_cache(array("*alert_notify*"));
		$rkey = $this->input->post("rid");
		$record= $this->fb_rest->get_record($table_name, $rkey);
		//fb_pr($record);
		if($record["status"] == "success")
		 {	
		 	$data['record'] = $record["result_set"];
			$data['rkey'] = $rkey;
			$this->load->view("settings_edit_content", $data);
		 }
		
	}
	function update(){
		$table_name="alert_notify";
		fb_clear_cache(array("*alert_notify*"));
		$form_data = $this->input->post();
		$form_data['updatedtime']=now();	
		//$built_date = $this->input->post("built_date", true);
		
		$rkey = $this->input->post("rkey");

		$oresult = $this->fb_rest->get_record($table_name, $rkey);
		$orecord = $oresult["result_set"];

		$result = $this->fb_rest->update_record($table_name,$form_data,$rkey);
		
//		print_r($result);
		if($result['status']=="success"){
		$this->session->set_flashdata('update_success',"Settings updated successfully");
		redirect('/settings');
		}else{
		$this->session->set_flashdata('update_failed',"Oops! there is something wrong, try again");
		redirect('/settings');
		}
	}

	public function eb_settings($meter_id){
		if($this->fb_rest->isloggedin()){
			if($this->fb_rest->has_accessable("list_dg")) {
				$data=array();
				$presult_data = array();
				$msg  = $this->iot_rest->getebSettings($meter_id);

				$this->load->view('include/header_view');
				$this->load->view('include/left-sidebar');
				if(empty($meter_id)){
					$this->load->view("layout/error", $data);
				}else if(!empty($msg['data'])){
					$data["source"] = $msg["data"][0]['_source'];
					$data['rkey'] = $msg["data"][0]["_id"];
					$this->load->view('eb_settings_edit',$data);	
				}
				else{
					$this->load->view('eb_settings');	
				}
				$this->load->view('include/footer');
		    } else {
				$this->load->view('alert/permission');
			}

		}else{
			redirect('/login');
		}		
	}

	public function dg_settings($meter_id,$dg_id){
		if($this->fb_rest->isloggedin()){
			if($this->fb_rest->has_accessable("list_dg")) {
				$data=array();
				$presult_data = array();
				$msg  = $this->iot_rest->getdgSettings($meter_id,$dg_id);

				$this->load->view('include/header_view');
				$this->load->view('include/left-sidebar');
				if(empty($meter_id)){
					$this->load->view("layout/error", $data);
				}else if(!empty($msg['data'])){
					$data["source"] = $msg["data"][0]['_source'];
					$data['rkey'] = $msg["data"][0]["_id"];
					$this->load->view('dg_edit_settings',$data);	
				}
				else{
					$this->load->view('dg_settings');	
				}
				$this->load->view('include/footer');
		    } else {
				$this->load->view('alert/permission');
			}

		}else{
			redirect('/login');
		}		
	}
	
}
