<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Meters extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->helper('date');
		$this->session->keep_flashdata('update_success');
		$this->session->keep_flashdata('update_failed');				
	}

	public function index()
	{
		if($this->fb_rest->isloggedin()){
			if($this->fb_rest->has_accessable("list_meter")) {
				$data=array();
				$presult_data = array();
				$rec_id = fb_fetch_id("meters"); // "350414";
				$result = $this->iot_rest->getmeter_list($rec_id);
				//print_r($result); exit();
				$this->load->view('include/header_view');
				$this->load->view('include/left-sidebar');
				if($result["status"] == "success"){
					
					
					$data["presult_data"] = $result["data"];
					$this->load->view('meters',$data);	
				}
				else{
					$this->load->view("layout/error", $data);
				}
				$this->load->view('include/footer');
		    } else {
				$this->load->view('alert/permission');
			}

		}else{
			redirect('/login');
		}
	}

	public function add_meter(){
		
		$table_name = "meters";
    	$config['upload_path'] = './meter-images/';
		$config['allowed_types'] = 'gif|jpg|png'; 

		$widgetArr = $this->config->item('widget','fb_boodskap');

        $this->load->library('upload', $config);

		$this->upload->do_upload('meter_image');

        /*if($this->upload->do_upload('meter_image')){
			$data = $this->upload->data();
     		$meter_image = $data['file_name'];
		}else{
			$error = array('error' => $this->upload->display_errors());
   			log_message('info', json_encode($error));
		}*/		

		$data = $this->upload->data();
     	$meter_image = $data['file_name'];

     	 $action = $this->input->post("action");
     	 $rid = $this->input->post("rid");
		 $meter_id = $this->input->post("meter_id");
		 $meter_name = $this->input->post("meter_name");
		 $meter_desc = $this->input->post("meter_desc");
		 $running_hrs = $this->input->post("running_hrs");
		 $kva = $this->input->post("kva");

     	 if($action=="update" && $rid){
     	 	$form_data = $this->input->post();
     	 	$form_data['updatedtime']=now();
     	 	$form_data['name']=$meter_name;
     	 	$form_data['description'] = $meter_desc;
     	 	$form_data['running_hrs'] = $running_hrs;
     	 	if($meter_image)
     	 	$form_data['image'] = $meter_image;
     	 	//print_r($form_data);  exit();
     	 	$result = $this->fb_rest->update_record($table_name,$form_data,$rid);
     	 }else{
			
			$idata = array("meter_id" => $meter_id, 
			"name" => $meter_name, 
			"description" => $meter_desc,
			"image" => $meter_image,
			"status" => "true",
			"kva" => $kva,
			"running_hrs"=>$running_hrs,
			"createdtime" => time(), 
			"updatedtime" => time());
			//print_r($idata); exit();
			$result = $this->fb_rest->create_record($table_name, $idata);   
				$darray= array();
					foreach ($widgetArr as  $value) {
					 $darray['callback'] = $value['callback'];
					 $darray['createdtime'] = time();
					 $darray['title'] = $value['title'];
					 $darray['position'] = $value['position'];
					 $darray['meter_id'] = $meter_id;
					 //print_r($darray);
				     $resultInsert = $this->fb_rest->create_record("pms_dashboard", $darray);   
				}			
     	 }

			if($result['status']=="success"){

				$this->session->set_flashdata('meter_success','meter added/updated successfully');
				redirect('/meters');
			}else{
				$this->session->set_flashdata('meter_failed','please try again later');
				redirect('/meters');
			}

	}	

	public function delete(){
		$table_name="meters";
		 $rkey = $this->input->post("rid");	 
		$result= $this->fb_rest->delete_record($table_name, $rkey);
		
		if($result['status']=="success"){
			$this->session->set_flashdata('delete_success',"meter deleted");
			redirect('/meters');
		}else{
			$this->session->set_flashdata('delete_failed',"issue deleting meter");
			redirect('/meters');
		}
	}

	public function updateStatus($rid,$status){
		$table_name="meters";
		$form_data =  array();
		if ($status=="false") {
			$form_data['status']="false";	
		}else{
			$form_data['status']="true";	
		}
		//print_r($form_data); exit();
		$rkey = $rid;
		$result = $this->fb_rest->update_record($table_name,$form_data,$rkey);
		//print_r($result); exit();
		if($result['status']=="success"){
			if($status=="false")
				$this->session->set_flashdata('update_success',"Meter has been de-activated successfully");
			else
				$this->session->set_flashdata('update_success',"Meter has been activated successfully");
			redirect('/meters');
		}else{
			$this->session->set_flashdata('update_failed',"meter status update failed");
			redirect('/meters');
		}
	}	

	public function addBcheck(){
		$table_name="bcheck_settings";
		//$tbl_id= 350401;
		$date = strtotime($this->input->post('next_date')) *1000;
		$hours=$this->input->post('next_hours');
		$meter_id= $this->input->post('meter_id');
		$dg_id= $this->input->post('dg_id');
		$rkey = $this->input->post('rkey');


		if($rkey){
			$form_data = $this->input->post();
     	 	$form_data['updatedtime']=now();
     	 	$form_data['date']=$date;
     	 	$form_data['hours'] = $hours;
     	 	//print_r($form_data);  exit();
     	 	$result = $this->fb_rest->update_record($table_name,$form_data,$rkey);
		}else{
			$idata = array("meter_id" => $meter_id, 
			"dg_id" => $dg_id, 
			"date" => $date, 
			"hours" => $hours,
			"createdtime" => now(), 
			"updatedtime" => now());

			$result = $this->fb_rest->create_record($table_name,$idata);
		}

		
		$this->output
				->set_content_type('application/json')
				->set_output(json_encode($result));
			
		
	}	

	public function getBcheck(){
	
		$meter_id = $this->input->post('meter_id');
		$response = array();
		$dg_id = $this->input->post('dg_id');
		$result = $this->iot_rest->getBcheck($meter_id,$dg_id);
		//print_r($result); exit();
		 if($result["status"] == "success" && !(empty($result['data']))){
			$response['date'] =  fb_convert_date_time_format($result['data']['_source']['date']);
			$response['hours'] = $result['data']['_source']['hours'];
			$response['rkey'] = $result['data']['_id'];
			$response['dg_id'] = $result['data']['_source']['dg_id'];
			$response['meter_id'] = $result['data']['_source']['meter_id'];
			$this->output
				->set_content_type('application/json')
				->set_output(json_encode($response));
			}else{
				$response['date'] =  0;
				$this->output
				->set_content_type('application/json')
				->set_output(json_encode($response));
			}
	}	

	public function bcheckAlert($meter_id){

		$result = $this->iot_rest->getBcheck($meter_id);
		$date =  fb_convert_jsdate($result['data']['date']);
		$alertdate= date("m/d/Y h:i:s a",strtotime($date." -3 day"));
		//echo "Date ".$result['data']['date']."<br>";
		$today =  date("m/d/Y h:i:s a",now());
		//$minDate = 
		if($today>$alertdate){
			echo "Trigger alert";
		}else{
			echo "RIP";
		}
		
	}

	public function managedg($meter_id){
		if($this->fb_rest->isloggedin()){
			if($this->fb_rest->has_accessable("list_dg")) {
				$data=array();
				$presult_data = array();
				$result = $this->iot_rest->getdg_list($meter_id);
				//print_r($result); exit();
				$this->load->view('include/header_view');
				$this->load->view('include/left-sidebar');
				if($result["status"] == "success"){
					
					
					$data["presult_data"] = $result["data"];
					$this->load->view('manage_dg',$data);	
				}
				else{
					$this->load->view("layout/error", $data);
				}
				$this->load->view('include/footer');
		    } else {
				$this->load->view('alert/permission');
			}

		}else{
			redirect('/login');
		}		
	}

	public function add_dg(){
		
		$table_name = "manage_dg";
    	
     	 $action = $this->input->post("action");
     	 $rid = $this->input->post("rid");
		 $dg_id = $this->input->post("dg_id");
		 $dg_name = $this->input->post("dg_name");
		 $dg_desc = $this->input->post("dg_desc");
		 $running_hrs = $this->input->post("running_hrs");
		 $kva = $this->input->post("kva");
		 $meter_id = $this->input->post("meter_id");
		 //print_r($this->input->post()); exit();
     	 if($action=="update" && $rid){
     	 	$form_data = $this->input->post();
     	 	$form_data['updatedtime']=now();
     	 	$form_data['dg_name']=$dg_name;
     	 	$form_data['description'] = $dg_desc;
     	 	$form_data['running_hrs'] = $running_hrs;
     	 	$form_data['kva'] = $kva;
     	 	//print_r($form_data);  exit();
     	 	$result = $this->fb_rest->update_record($table_name,$form_data,$rid);
     	 }else{
			
			$idata = array("meter_id" => $meter_id, 
			"dg_id" => $dg_id, 
			"dg_name" => $dg_name, 
			"description" => $dg_desc,
			"status" => "true",
			"kva" => $kva,
			"running_hrs"=>$running_hrs,
			"createdtime" => time(), 
			"updatedtime" => time());
			$result = $this->fb_rest->create_record($table_name, $idata);     	 	
     	 }

			if($result['status']=="success"){
				$this->session->set_flashdata('meter_success','DG added/updated successfully');
				redirect('meters/managedg/'.$meter_id);
			}else{
				$this->session->set_flashdata('meter_failed','please try again later');
				redirect('meters/managedg/'.$meter_id);
			}

	}

	public function dgupdateStatus($rid,$status,$meter_id){
		$table_name="manage_dg";
		$form_data =  array();
		if ($status=="false") {
			$form_data['status']="false";	
		}else{
			$form_data['status']="true";	
		}
		//print_r($form_data); exit();
		$rkey = $rid;
		$result = $this->fb_rest->update_record($table_name,$form_data,$rkey);
		//print_r($result); exit();
		if($result['status']=="success"){
			if($status=="false")
				$this->session->set_flashdata('update_success',"DG has been de-activated successfully");
			else
				$this->session->set_flashdata('update_success',"DG has been activated successfully");
			redirect('meters/managedg/'.$meter_id);
		}else{
			$this->session->set_flashdata('update_failed',"DG status update failed");
			redirect('meters/managedg/'.$meter_id);
		}
	}

	public function deleteDG(){
		$table_name="manage_dg";
		$rkey = $this->input->post("rid");
		$meter_id = $this->input->post("meter_id");	
		$result= $this->fb_rest->delete_record($table_name, $rkey);
		
		if($result['status']=="success"){
			$this->session->set_flashdata('delete_success',"meter deleted");
			redirect('meters/managedg/'.$meter_id);
		}else{
			$this->session->set_flashdata('delete_failed',"issue deleting meter");
			redirect('meters/managedg/'.$meter_id);
		}
	}	

	public function fuelSettings(){

		$table_name="dgfuel_setting";
		//$tbl_id= 350401;
		fb_clear_cache(array("*dgfuel_setting*"));
		$form_data['twentyfive'] = $this->input->post('qtr_ltr');
		$form_data['fifty'] = $this->input->post('half_ltr');
		$form_data['seventyfive'] = $this->input->post('threefour_ltr');
		$form_data['hundred'] = $this->input->post('full_ltr');
		$form_data['hundredmore'] = $this->input->post('fullover_ltr');
		$form_data['meter_id'] = $this->input->post('fuel_meterid');
		$form_data['dg_id'] = $this->input->post('fueldg_id');
		$form_data['createdtime']=time();
		$form_data['updatedtime']=time();
		//print_r($form_data); exit();
		$updateId = $this->input->post('updateId');
		if($updateId=="")
		$result = $this->fb_rest->create_record($table_name,$form_data);
		else
		$result = $this->fb_rest->update_record($table_name,$form_data,$updateId);
//		print_r($result);
		if($result['status']=="success"){
			echo "success";
		}else{
			echo "failed";
		}
	}

	public function getfuelSettings(){
		$table_name="dgfuel_setting";
		fb_clear_cache(array("*dgfuel_setting*"));
		$id = $this->input->post('meter_id');
		$dg_id = $this->input->post('dg_id');
		$fuelSettings = $this->iot_rest->getfuelSettings($id,$dg_id); 

		$arrData = array_merge(array("data"=>$fuelSettings['data']), array("_id"=>$fuelSettings['id']));
		//print_r($arrData); exit();

		if($fuelSettings['status']=="success"){

			$this->output
			->set_content_type('application/json')
			->set_output(json_encode($arrData));
		}
	}

	public function eb_settings($meter_id){
		if($this->fb_rest->isloggedin()){
			if($this->fb_rest->has_accessable("list_dg")) {
				$data=array();
				$presult_data = array();
				$msg  = $this->iot_rest->getebSettings($meter_id);
				$data["result_set"] = $msg["result_set"];
	
				$this->load->view('include/header_view');
				$this->load->view('include/left-sidebar');
				if(empty($meter_id)){
					$this->load->view("layout/error", $data);
				}
				if($data['result_set'][0]['_id']){
					$data["presult_data"] = $result["data"];
					$this->load->view('eb_settings_edit',$data);	
				}
				else{
					$this->load->view('eb_settings',$data);	
				}
				$this->load->view('include/footer');
		    } else {
				$this->load->view('alert/permission');
			}

		}else{
			redirect('/login');
		}		
	}

}
?>