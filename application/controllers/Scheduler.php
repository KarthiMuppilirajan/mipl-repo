<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Scheduler extends MY_Controller {

	public function __construct() {
		parent::__construct();
		if(!$this->fb_rest->isloggedin()) {
			$this->fb_rest->cli_login();
		}
	}

	public function bcheckAlert(){

		$result = $this->iot_rest->getallBcheck();
			//print_r($result); exit();
			$bcheck= $result['data'];
			foreach ($bcheck as $row) {
				 $meter_id = $row['_source']['meter_id'];
				 $dg_id = $row['_source']['dg_id'];
				 $endDate = $row['_source']['date']/1000;
				 $endHours = $row['_source']['hours'];
				 $lastupdatetime = $row['_source']['updatedtime']*1000;
				 $now=time()*1000;
				 $date_raw = date('d-m-Y H:i:s', $endDate);
				 $first_date = strtotime($date_raw);
				 $checkDate = strtotime('-3 day', $first_date);
				
				 $tbl_bcheck = fb_fetch_id("manage_dg");
				 $qpms = array("size" => 1, "from" => 0, "orderfld" => 'createdtime', "orderdir" => 'asc',"meter_id"=>$meter_id,"dg_id"=>$dg_id);
			 	 $query_bcheck = $this->parser->parse('query/query_by_dg', $qpms, true);   
 				 $CI =& get_instance();
				 $resultBcheck = $CI->iot_rest->get_query_result($tbl_bcheck, $query_bcheck);
		
				 $old_running_hrs = $resultBcheck['result_set'][0]['_source']['running_hrs']; 
				 
				
				 $query_str = $this->parser->parse('query/dg_get_runninghrs', array("meter_id"=>$meter_id,"dg_id"=>$dg_id,"min_date"=>$lastupdatetime,"max_date"=>$now), true); 

				 $tbl_id = fb_fetch_id("genset_activity"); // "350409";
				 $CI =& get_instance();
				 $result = $CI->iot_rest->get_query_agg_result($tbl_id, $query_str);
				 $aggs = $result['aggs'];
				// print_r($aggs);
				 $crunning_hrs = 0;
				 if(!empty($aggs))
				 {
					$key =0;
					foreach($aggs as $arr)
					{
						if($arr['key']>0)
						$key+=$arr['key'];
					}
					$totalMinutes = $key/60;
					$hours = round($totalMinutes / 60);
					$crunning_hrs = $hours;
					//$running_hours = $old_running_hrs + $crunning_hrs; 
				 }
				 $alertcheckHrs = $old_running_hrs+$endHours; 
				 $remainingHrs = $alertcheckHrs - $crunning_hrs; 
				 
				 if($remainingHrs<=72){
					$this->send_mail_bcheck($meter_id,$dg_id,'Hour Reminder',$remainingHrs); 
				 }
				 if($crunning_hrs>=$alertcheckHrs){
					$this->send_mail_bcheck($meter_id,$dg_id,'Hour exceed',$date_raw);
				 }
				 
				 $currTime = time() + 19800; 
				   
				if($currTime>=$checkDate){
					$this->send_mail_bcheck($meter_id,$dg_id,'Date exceed',$date_raw);
				}else if($checkDate <$currTime){
					$this->send_mail_bcheck($meter_id,$dg_id,'Reminder',$date_raw);
				}
				   

			}
			
	}

	public function checkmeterStatus(){
		$rec_id = fb_fetch_id("meters"); // "350414";
		$result = $this->iot_rest->activemeter_list($rec_id);
		//print_r($result); exit();
		foreach ($result['data'] as $row) {
			 $update_data = array();

			 $meter_id = $row['_source']['meter_id']; 
			 $status = $this->iot_rest->getmeterStatus($meter_id);
			 $rkey = $status['data']['_id'];
			 //print_r($status); exit();
			 $update_data['meter_id'] = $status['data']['_source']['meter_id'];
			
			 if($status['data']['_source']['createdtime']){
			 	 $update_data['createdtime'] =$status['data']['_source']['createdtime'];
			 }else{
			 	$update_data['createdtime'] = now()."000";
			 }

			 $update_data['record_id'] = $status['data']['_source']['record_id'];

			 if($status['data']['_source']['dg_id']){
			 	$update_data['dg_id'] = $status['data']['_source']['dg_id'];
			 }
			 
			 $lastrecord = $this->iot_rest->get_power_last($meter_id);
			//print_r($status); 
			 $lastRecordid = $lastrecord['data']['_id'];
			if($status["status"]=="success" && $status['data']['_source']['record_id'] && $lastrecord['status']=="success" && $lastrecord['data']['_id']){
				//print_r($status['data']);
				//echo $status['data']['_source']['record_id']."<br>";
				//echo $lastRecordid."<br>";
				if($status['data']['_source']['record_id'] == $lastRecordid){
					//echo $rkey."<br>";
					$update_data['status'] = 2;
					$result = $this->fb_rest->update_record("meter_status",$update_data,$rkey);
					//$this->send_mail($meter_id);
				}
			}
		}
	}
	
	public function meterdownStatus(){
		$rec_id = fb_fetch_id("meters"); // "350414";
		$result = $this->iot_rest->activemeter_list($rec_id);
		//print_r($result); exit();
		foreach ($result['data'] as $row) {
			 $update_data = array();

			 $meter_id = $row['_source']['meter_id']; 
			 $status = $this->iot_rest->getmeterStatus($meter_id);
			 $rkey = $status['data']['_id'];
			 print_r($status); 
			 $update_data['meter_id'] = $status['data']['_source']['meter_id'];
			  $statusupated= substr($status['data']['_source']['updatedtime'],0,10); 
		 
			  
			
			 if($status['data']['_source']['createdtime']){
			 	 $update_data['createdtime'] =$status['data']['_source']['createdtime'];
			 }else{
			 	$update_data['createdtime'] = now()."000";
			 }

			 $update_data['record_id'] = $status['data']['_source']['record_id'];

			 if($status['data']['_source']['dg_id']){
			 	$update_data['dg_id'] = $status['data']['_source']['dg_id'];
			 }
			 
			 $lastrecord = $this->iot_rest->get_power_last($meter_id);
			  print_r($lastrecord); exit();
			  $lastRecordid = $lastrecord['data']['_id'];
			  $lastcreadtedTime = substr($lastrecord['data']['_source']['createdtime'],0,10);  

			  $diff= round(abs($statusupated - $lastcreadtedTime) / 60,2);
			 
			if($status["status"]=="success" && $status['data']['_source']['record_id'] && $lastrecord['status']=="success" && $lastrecord['data']['_id']){
	
				if($status['data']['_source']['record_id'] == $lastRecordid && $diff>=10){
					$this->send_mail($meter_id);
				}else{
					echo "Meter(s) running fine";
				}
			}
		}
	}
	
	public function getnotes_list($rec_id){
		$tbl_id = $rec_id;
		$from = 0;
		$size = 1000;
		$orderfld = "createdtime";
		$orderdir = "desc";
	
	
		$query_str = '{  "query": { "match":{"send_status":"not send"} },  "size" : '.$size.',  "from": '.$from.',  "sort": { "'.$orderfld.'" : {"order" : "'.$orderdir.'"} } }';

		$CI =& get_instance();
		$result = $CI->iot_rest->get_query_result($tbl_id, $query_str);
		//print_r($result); exit();
		if( $result['total_count']>0 ){
			$src = isset($result["result_set"]) ? $result["result_set"] : array();
			$msg = array("status" => "success", "message" => "Message Data fetched successfully", 
				 "data" => $src);
				return $msg;
		}else{
			echo "No data to send";
		}	
	}
	
	public function reminder(){
		 
		$rec_id = fb_fetch_id("notes"); // "350435";
		$result = $this->getnotes_list($rec_id);
		//print_r($result);
		 foreach($result['data'] as $row){
              $rkey = $row['_id'];
			  $source = $row['_source'];
			 
			 $update_data = array();
			 $update_data['date'] = $source['date'];
			 $update_data['notes_title'] = $source['notes_title'];
			 $update_data['notes_description'] = $source['notes_description'];
			 $update_data['createdtime'] = $source['createdtime'];
			 $update_data['updatedtime'] = $source['updatedtime'];
			 $update_data['email'] = $source['email'];
			 $update_data['status'] = $source['status'];
			 //$update_data['send_status'] = $source['send_status'];
			 
			  $rdate = fb_convert_date_time_format($source['date']); 
			  $checkTime = strtotime($rdate);
			   $ctime = time()+ 19800; 
	
			 if($ctime>=$checkTime){
				 $update_data['send_status'] = 'sent';
				 $result_update = $this->fb_rest->update_record("notes",$update_data,$rkey);
				 //print_r($result_update);
				 $this->reminder_send_mail($source['email'],$source['notes_title'],$source['notes_description']);
			 }else{
				 echo "No reminder(s) to send";
			 }
			 
		 }
              
		
	}
	
	public function send_mail($meter_id){
		 $from_email = "iot-admin@fourbends.com"; 
         //$to_email = "jothikannan@fourbends.com"; 
		 $to_email = array('jothikannan@fourbends.com', 'kamesh@fourbends.com', 'pitchaimuthu@fourbends.com');
		 $subject = "Meter Down - $meter_id"; 
         //Load email library 
         $this->load->library('email'); 
		 $this->email->set_mailtype("html");
         $this->email->from($from_email, 'Forubends IOT'); 
         $this->email->to($to_email);
         $this->email->subject($subject); 
		 $data= array();
		 $data['meter_id'] = $meter_id;
         $this->email->message($this->load->view('mail/meter_down', $data, true)); 
		  if($this->email->send()) 
          echo " Email sent successfully."; 
         else 
          echo "Error in sending Email."; 
   
	}
	
	public function send_mail_bcheck($meter_id,$dg_id,$type,$endDate){
		 $from_email = "iot-admin@fourbends.com"; 
         $to_email = array('jothikannan@fourbends.com', 'kamesh@fourbends.com', 'pitchaimuthu@fourbends.com');
		 
          $meter =  $this->iot_rest->getmeterName($meter_id); 
          $meter_name = $meter['data'];
		  
		 $subject = "B-Check Reminder - $meter_name"; 
         //Load email library 
         $this->load->library('email'); 
		 $this->email->set_mailtype("html");
         $this->email->from($from_email, 'Forubends IOT'); 
         $this->email->to($to_email);
         $this->email->subject($subject); 
		 $data= array();
		 $data['meter_id'] = $meter_id;
		 $data['dg_id'] = $dg_id;
		 $data['type'] = $type;
		 $data['endDate'] = $endDate;
         $this->email->message($this->load->view('mail/b-check', $data, true)); 
		  if($this->email->send()) 
          echo " Email sent successfully."; 
         else 
          echo "Error in sending Email."; 
   
	}
	
	public function reminder_send_mail($to_email,$subject,$mail){
		 $from_email = "iot-admin@fourbends.com"; 
         //$to_email = "jothikannan@fourbends.com"; 
		 $subject = "Notes Reminder - $subject"; 
         //Load email library 
         $this->load->library('email'); 
		 $this->email->set_mailtype("html");
         $this->email->from($from_email, 'Forubends IOT'); 
         $this->email->to($to_email);
         $this->email->subject($subject); 
		 $data= array();
		 $data['mail'] = $mail;
         $this->email->message($this->load->view('mail/notes_reminder', $data, true)); 
		  if($this->email->send()) 
          echo " Email sent successfully."; 
         else 
          echo "Error in sending Email."; 
   
	}

	public function dg_running_report(){

		
			$data=array();
			$presult_data = array();
				$from = 0;
				$tot_cnt = 99999;
		
				$result_fuel = $this->iot_rest->new_fuel_records($from,$tot_cnt);
		
				$meterList = $this->iot_rest->getMeters();
				$meterNames = $meterList['data'];
				if($result_fuel["status"] == "success"){
					for($i=0;$i<count($result_fuel['data']);$i++)
					{
						 $rec_id = $result_fuel['data'][$i]['_id'];  
						 $source = $result_fuel['data'][$i]['_source'];
					
							
							$rtime= hourFormat($source["last_on_time"],$source["last_off_time"]);
							//$fuel= $source["running_hours"];
		                    $meter_id =$source['device_id'];
		                    $fuel =$this->iot_rest->dgFuel($source["device_id"],$source["last_on_time"],$source["last_off_time"],$source["dg_id"]);
							$consumed = round($fuel,2);
							
							 $last_on_time = $source['last_on_time'];
							 $last_off_time = $source['last_off_time'];
		                   
		        			 if($last_on_time && $last_off_time){
			                    $data['fuel_consumed'] = $consumed;
			                    $result = $this->fb_rest->update_record('genset_activity',$data,$rec_id);
			                }
	
					}
					
				}
			}

}