<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Name:    Fourbends - Boodskap API
 * Author:  Fourbends Dev Team
 *          contactus@fourbends.com
 * Url:    http://www.fourbends.com/
 *
 *
 * Created:  02.03.2018
 *
 * Description:  This file is used to refer the tables list in boodskap.
 * Original Author name has been kept but that does not mean that the method has not been modified.
 *
 * Requirements: PHP5 or above
 *
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$config['tables'] = array("eb_status"=>"350232",  "devices" => "350404", "power_manage" => "350225",
"alerts" => "350226","rooms" => "350407","genset" => "350227","genset_activity" =>"350228",
"genset_list" =>"350222","power-meter"=>"350230" , "alert_settings" => "350223", 
"alert_notify" => "350224", "sample_genset_activity" => "350410","alert_settings_dg"=>"350229",
"meters"=>"350231","bcheck_settings"=>"350233","dgfuel_setting"=>"350234", "g_notifications" => "350221",
"kwh_log"=>"350235","notes"=>"350236", "alert_status"=>"350237","manage_dg"=>"350238","meter_status"=>"350239","pms_dashboard"=>"350240");

$config['devicesArr'] = array(
   "Light" => array( "key" => "light", "value" => "Light","icon" =>"light-bulb-idle.png"), 
    "Fan" => array( "key" => "fan", "value" => "Fan", "icon" =>"fan-idle.png"),
    "AC" => array( "key" => "ac", "value" => "AC","icon" =>"air-conditioner-idle.png"),
    "Microwave oven" => array( "key" => "oven", "value" => "Microwave oven","icon" =>"microwave-idle.png" ), 
    "Washing Machine" => array( "key" => "wm", "value" => "Washing Machine","icon" =>"washing-machine-idle.png" ), 
    "Lamp" => array( "key" => "lamp", "value" => "Lamp","icon" =>"lamp-idle.png" ), 
    "Water heater" => array( "key" => "heater", "value" => "Water heater","icon" =>"water-heater-idle.png" ), 
    "Fridge" => array( "key" => "fridge", "value" => "Fridge","icon" =>"fridge-idle.png" ), 
);

$config["devices"] = array("amps", "device_id", "device_name", "device_status", "voltage", "hrs", "watts", "device_type","createdtime", "updatedtime"); 
					
$config["power_manage"] = array("amps", "phase", "source", "voltage", "createdtime", "updatedtime");

$config["genset"] = array("device_id","device_name","device_type","status","watts","createdtime","updatedtime");

$config["genset_activity"] = array("device_id","device_name","last_on_time", "last_off_time","dg_id","fuel_consumed","hours");

$config["rooms"] = array("room_id", "room_name", "room_status", "timeout", "guest_access", "last_active", "timeout_last_active", "timeout_enable", "autowakeuptime", "autowakeuplastran", "createdtime", "updatedtime");

$config["alerts"] = array( "meter_id", "alert_state", "alert_fld", "alert_value", "fld_rid", "alert_reason", "alert_type", "msg", "alert_src", "createdtime", "updatedtime");

$config["genset_list"] = array( "name", "type", "rpm", "minVal", "maxVal","status","rHour","fuel_consumption","createdtime", "updatedtime" );

$config["power-meter"] = array("bc","bp","bv","byv","fre","gen","pf","rbv","rc","rp","rv","tc","tp","tpv","tv","yc","yp","yrv","createdtime","yv","created","meter_id","gen1","gen2","gen3","dg_id");

$config["alert_settings"] = array("bcpmax", "bcpmin", "bppmax", "bppmin", "bspmax", "bspmin", "btpmax", "btpmin","createdtime", "fremax", "fremin", "pfmax", "pfmin", "rcpmax", "rcpmin", "rppmax", "rppmin", "rspmax", "rspmin", "rtpmax","rtpmin", "spmax", "spmin", "tcpmax", "tcpmin", "tpmax", "tpmin", "tppmax", "tppmin", "updatedtime", "ycpmax", "ycpmin", "yppmax", "yppmin", "yspmin", "ytpmax", "ytpmin", "yspmax","kvamin","kvamax","meter_id");

$config["alert_settings_dg"] = array("dgbcpmax", "dgbcpmin", "dgbppmax", "dgbppmin", "dgbspmax", "dgbspmin", "dgbtpmax", "dgbtpmin","createdtime", "dgfremax", "dgfremin", "dgpfmax", "dgpfmin", "dgrcpmax", "dgrcpmin", "dgrppmax", "dgrppmin", "dgrspmax", "dgrspmin", "dgrtpmax","dgrtpmin", "dgspmax", "dgspmin", "dgtcpmax", "dgtcpmin", "dgtpmax", "dgtpmin", "dgtppmax", "dgtppmin", "updatedtime", "dgycpmax", "dgycpmin", "dgyppmax", "dgyppmin", "dgyspmin", "dgytpmax", "dgytpmin", "dgyspmax","dgkvamin","dgkvamax","meter_id","dg_id");

$config["alert_notify"] = array("createdtime", "updatedtime" , "name", "ph_no", "email");

$config["sample_genset_activity"] = array("genset_id","genset_name","last_on_time", "last_off_time");

$config["meters"] = array("meter_id","name","description", "status","image","createdtime","updatedtime","kva","running_hrs");

$config["eb_status"]=array("status","status_on","status_off","meter_id","dg_id");

$config["bcheck_settings"]=array("date","hours","createdtime","updatedtime","meter_id","dg_id");

$config["dgfuel_setting"] = array("twentyfive","fifty","seventyfive","hundred","hundredmore","createdtime","updatedtime","meter_id","dg_id");

$config["g_notifications"] = array("createdtime", "custom_pms", "nfld_name", "nfld_uid", "nfld_value", "nmsg", "nstate", "ntype", "project_name", "project_uid", "updatedtime","meter_id");

$config["notes"]=array("notes_title","notes_description","date","createdtime","updatedtime","email","status","send_status");

$config["alert_status"]=array("alert_name","alert_flag","createdtime","updatedtime");

$config["manage_dg"]=array("meter_id","dg_name","dg_id","kva","running_hrs","createdtime","updatedtime","description","status");

$config["meter_status"]=array("meter_id","status","createdtime","updatedtime","msg_id");

$config["pms_dashboard"]=array("meter_id","title","position","callback","createdtime");

$config["timezone"] = "Asia/Kolkata";

$config["user_token"] =  "EEFRJGPUUH:DCD095nfvcQd";

$config["fb_rapi_burl"] = "https://fb-rest-api.4blabs.com/";

$config["fb_rapi_key"] = "97JB8S0JIHHZOFF1GOI3";

$config["fb_rsec_key"] = "7KGWZ3QIWCU9RHPGFOQX";

$config['gaAccount'] ="UA-134366596-4";

$config['widget'] = array( 
	array("callback"=>"FrequencyGauge","title"=>"Frequency Gauge","position"=>"1", "meter_id"=>''),
	array("callback" => "gaugeAmp", "title" => "Amp gauge","position" => "2","meter_id"=>''),
	array("callback" => "gaugeVoltage", "title" => "Voltage gauge - Single phase","position" => "3","meter_id"=>''),
	array("callback" => "gaugeKVA", "title" => "KVA Gauge","position" => "4","meter_id"=>''),
	array("callback" => "ViewVoltage", "title" => "Total Three Phase Voltage","position" => "5","meter_id"=>''),
	array("callback" => "SinglePower", "title" => "Single Phase power","position" => "6","meter_id"=>''),
	array("callback" => "ViewAmp", "title" => "Current","position" => "7","meter_id"=>''),
	array("callback" => "KWH", "title" => "KWH","position" => "8","meter_id"=>''),
	array("callback" => "dg_KWH", "title" => "KWH DG","position" => "9","meter_id"=>''),
	array("callback" => "PowerFactor", "title" => "Power Factor","position" => "10","meter_id"=>''),
	array("callback" => "3KVA", "title" => "Three Phase KVA","position" => "11","meter_id"=>''),
	array("callback" => "Frequency", "title" => "Frequency","position" => "12","meter_id"=>''),
	);
