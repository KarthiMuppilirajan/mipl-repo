<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

// Google Analytics / Web Tracking
// https://developers.google.com/analytics/devguides/collection/gajs/eventTrackerGuide
// 
// This method generates the track pageview code of Google Analytics

if (!function_exists('initGA'))
{
    function initGA($gaAccount){
        if( empty($gaAccount) ) {
            return false;
        }

        $ga_code = "
            <script async src='https://www.googletagmanager.com/gtag/js?id={$gaAccount}'></script>
            <script>
              window.dataLayer = window.dataLayer || [];
              function gtag(){dataLayer.push(arguments);}
              gtag('js', new Date());

              gtag('config', '{$gaAccount}');
            </script>";

        return $ga_code;
    }
}

// This method generates the track event code of Google Analytics

if (!function_exists('trackEventGA'))
{
    function trackEventGA($category, $action, $label){
        $category = addslashes($category);
        $action   = addslashes($action);
        $label    = addslashes($label);
       // $ga_code  = "onclick=\"_gaq.push(['_trackEvent', '{$category}', '{$action}', '{$label}']);\"";
        $ga_code = "onclick=\"gtag('event', 'click', { 'event_category': '{$category}',event_label :'{$label}' });\""; 

        return $ga_code;
    }
}
if (!function_exists('gen_uuid'))
{
    function gen_uuid() {
        return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
        mt_rand( 0, 0xffff ),
        mt_rand( 0, 0x0fff ) | 0x4000,
        mt_rand( 0, 0x3fff ) | 0x8000,
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
        );
    }
}
if(!function_exists('sendgaEvent')){
    function sendgaEvent($gaAccount,$category, $action, $label){
         $data = array(
        'v' => 1,
        'tid' => $gaAccount,
        'cid' => gen_uuid(),
        't' => 'event'
        );

        $data['ec'] = $category;
        $data['ea'] = $action;
        $data['el'] = $label;
        $data['ev'] = "34";
        $data['uip']= $_SERVER['REMOTE_ADDR'];
        $data['ua']=  $_SERVER['HTTP_USER_AGENT'];

        //$url = 'https://www.google-analytics.com/debug/collect';
        $url = 'https://www.google-analytics.com/collect';
        $content = http_build_query($data);
        $content = utf8_encode($content);

        $ch = curl_init();
        curl_setopt($ch,CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_HTTPHEADER,array('Content-type: application/x-www-form-urlencoded'));
        curl_setopt($ch,CURLOPT_HTTP_VERSION,CURL_HTTP_VERSION_1_1);
        curl_setopt($ch,CURLOPT_POST, TRUE);
        curl_setopt($ch,CURLOPT_POSTFIELDS, $content);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_exec($ch);
        //print_r($result); exit();
        curl_close($ch);       

    }
}

/* End of file google_analytics_helper.php */
/* Location: ./application/helpers/google_analytics_helper.php */