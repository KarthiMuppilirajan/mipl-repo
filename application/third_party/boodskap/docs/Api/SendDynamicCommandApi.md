# Swagger\Client\SendDynamicCommandApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**sendDynamicCommand**](SendDynamicCommandApi.md#sendDynamicCommand) | **POST** /command/send/{atoken}/{commandId} | Send a Dynamic command to one device


# **sendDynamicCommand**
> \Swagger\Client\Model\CommandStatus[] sendDynamicCommand($atoken, $command_id, $command)

Send a Dynamic command to one device

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\SendDynamicCommandApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in user
$command_id = 56; // int | Command Identifier
$command = new \Swagger\Client\Model\Command(); // \Swagger\Client\Model\Command | Command JSON object

try {
    $result = $apiInstance->sendDynamicCommand($atoken, $command_id, $command);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SendDynamicCommandApi->sendDynamicCommand: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **command_id** | **int**| Command Identifier |
 **command** | [**\Swagger\Client\Model\Command**](../Model/Command.md)| Command JSON object |

### Return type

[**\Swagger\Client\Model\CommandStatus[]**](../Model/CommandStatus.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

