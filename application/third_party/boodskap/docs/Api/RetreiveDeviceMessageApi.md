# Swagger\Client\RetreiveDeviceMessageApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getDeviceMessage**](RetreiveDeviceMessageApi.md#getDeviceMessage) | **GET** /message/get/{atoken}/{muid} | Retreive Device Message


# **getDeviceMessage**
> \Swagger\Client\Model\DeviceMessage getDeviceMessage($atoken, $muid)

Retreive Device Message

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\RetreiveDeviceMessageApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in user
$muid = "muid_example"; // string | message UUID

try {
    $result = $apiInstance->getDeviceMessage($atoken, $muid);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RetreiveDeviceMessageApi->getDeviceMessage: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **muid** | **string**| message UUID |

### Return type

[**\Swagger\Client\Model\DeviceMessage**](../Model/DeviceMessage.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

