# Swagger\Client\RetreiveDomainDeviceGroupPropertyApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getDomainDeviceGroupProperty**](RetreiveDomainDeviceGroupPropertyApi.md#getDomainDeviceGroupProperty) | **GET** /domain/device/group/property/get/{atoken}/{gid}/{name} | Retreive Domain Device Group Property


# **getDomainDeviceGroupProperty**
> \Swagger\Client\Model\DomainDeviceGroupProperty getDomainDeviceGroupProperty($atoken, $gid, $name)

Retreive Domain Device Group Property

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\RetreiveDomainDeviceGroupPropertyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in domain
$gid = 56; // int | Group ID
$name = "name_example"; // string | 

try {
    $result = $apiInstance->getDomainDeviceGroupProperty($atoken, $gid, $name);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RetreiveDomainDeviceGroupPropertyApi->getDomainDeviceGroupProperty: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in domain |
 **gid** | **int**| Group ID |
 **name** | **string**|  |

### Return type

[**\Swagger\Client\Model\DomainDeviceGroupProperty**](../Model/DomainDeviceGroupProperty.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

