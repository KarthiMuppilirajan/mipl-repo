# Swagger\Client\BroadcastRawCommandApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**broadcastRawCommand**](BroadcastRawCommandApi.md#broadcastRawCommand) | **POST** /command/raw/broadcast/{atoken}/{type} | Broadcast raw command to multiple devices


# **broadcastRawCommand**
> \Swagger\Client\Model\CommandStatus[] broadcastRawCommand($atoken, $type, $command)

Broadcast raw command to multiple devices

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\BroadcastRawCommandApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in user
$type = "type_example"; // string | data type
$command = new \Swagger\Client\Model\BroadcastCommand(); // \Swagger\Client\Model\BroadcastCommand | BroadcastCommand JSON object

try {
    $result = $apiInstance->broadcastRawCommand($atoken, $type, $command);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling BroadcastRawCommandApi->broadcastRawCommand: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **type** | **string**| data type |
 **command** | [**\Swagger\Client\Model\BroadcastCommand**](../Model/BroadcastCommand.md)| BroadcastCommand JSON object |

### Return type

[**\Swagger\Client\Model\CommandStatus[]**](../Model/CommandStatus.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

