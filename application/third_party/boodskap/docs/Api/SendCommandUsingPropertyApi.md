# Swagger\Client\SendCommandUsingPropertyApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**sendCommandUsingProperty**](SendCommandUsingPropertyApi.md#sendCommandUsingProperty) | **POST** /command/property/send/{atoken}/{deviceId}/{commandId}/{property} | Send a Command using Property


# **sendCommandUsingProperty**
> \Swagger\Client\Model\CommandStatus sendCommandUsingProperty($atoken, $device_id, $command_id, $property, $command)

Send a Command using Property

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\SendCommandUsingPropertyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in user
$device_id = "device_id_example"; // string | device id
$command_id = 56; // int | static command id
$property = "property_example"; // string | property name
$command = new \Swagger\Client\Model\PropertyCommand(); // \Swagger\Client\Model\PropertyCommand | PropertyCommand JSON object

try {
    $result = $apiInstance->sendCommandUsingProperty($atoken, $device_id, $command_id, $property, $command);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SendCommandUsingPropertyApi->sendCommandUsingProperty: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **device_id** | **string**| device id |
 **command_id** | **int**| static command id |
 **property** | **string**| property name |
 **command** | [**\Swagger\Client\Model\PropertyCommand**](../Model/PropertyCommand.md)| PropertyCommand JSON object |

### Return type

[**\Swagger\Client\Model\CommandStatus**](../Model/CommandStatus.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

