# Swagger\Client\SendStaticCommandUsingTemplateApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**sendStaticCommandUsingTemplate**](SendStaticCommandUsingTemplateApi.md#sendStaticCommandUsingTemplate) | **POST** /command/template/send/{atoken}/{deviceId}/{commandId}/{templateId}/{system} | Send a Static Command to one device using Template


# **sendStaticCommandUsingTemplate**
> \Swagger\Client\Model\CommandStatus sendStaticCommandUsingTemplate($atoken, $device_id, $command_id, $template_id, $system, $merge_content)

Send a Static Command to one device using Template

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\SendStaticCommandUsingTemplateApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in user
$device_id = "device_id_example"; // string | device id
$command_id = 56; // int | static command id
$template_id = "template_id_example"; // string | template id
$system = true; // bool | true for system template
$merge_content = "merge_content_example"; // string | Merge Content JSON object

try {
    $result = $apiInstance->sendStaticCommandUsingTemplate($atoken, $device_id, $command_id, $template_id, $system, $merge_content);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SendStaticCommandUsingTemplateApi->sendStaticCommandUsingTemplate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **device_id** | **string**| device id |
 **command_id** | **int**| static command id |
 **template_id** | **string**| template id |
 **system** | **bool**| true for system template |
 **merge_content** | **string**| Merge Content JSON object |

### Return type

[**\Swagger\Client\Model\CommandStatus**](../Model/CommandStatus.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

