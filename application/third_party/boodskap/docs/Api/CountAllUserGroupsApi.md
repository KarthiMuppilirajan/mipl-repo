# Swagger\Client\CountAllUserGroupsApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**countAllUserGroups**](CountAllUserGroupsApi.md#countAllUserGroups) | **POST** /user/group/count/{atoken}/{userId} | Count All User Groups


# **countAllUserGroups**
> \Swagger\Client\Model\Count countAllUserGroups($atoken, $user_id)

Count All User Groups

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\CountAllUserGroupsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in user
$user_id = "user_id_example"; // string | User ID

try {
    $result = $apiInstance->countAllUserGroups($atoken, $user_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling CountAllUserGroupsApi->countAllUserGroups: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **user_id** | **string**| User ID |

### Return type

[**\Swagger\Client\Model\Count**](../Model/Count.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

