# Swagger\Client\ListDomainUserGroupMembersApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**listDomainUserGroupMembers**](ListDomainUserGroupMembersApi.md#listDomainUserGroupMembers) | **GET** /domain/user/group/listmembers/{atoken}/{gid}/{pageSize} | List Domain User Group Members


# **listDomainUserGroupMembers**
> \Swagger\Client\Model\User[] listDomainUserGroupMembers($atoken, $gid, $page_size, $direction, $did)

List Domain User Group Members

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\ListDomainUserGroupMembersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in user
$gid = 56; // int | Group id to list the users from
$page_size = 56; // int | Maximum number of users to be listed
$direction = "direction_example"; // string | If direction is specified, **did** is required
$did = "did_example"; // string | Last or First user id of the previous list operation, **required** if **direction** is specified

try {
    $result = $apiInstance->listDomainUserGroupMembers($atoken, $gid, $page_size, $direction, $did);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ListDomainUserGroupMembersApi->listDomainUserGroupMembers: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **gid** | **int**| Group id to list the users from |
 **page_size** | **int**| Maximum number of users to be listed |
 **direction** | **string**| If direction is specified, **did** is required | [optional]
 **did** | **string**| Last or First user id of the previous list operation, **required** if **direction** is specified | [optional]

### Return type

[**\Swagger\Client\Model\User[]**](../Model/User.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

