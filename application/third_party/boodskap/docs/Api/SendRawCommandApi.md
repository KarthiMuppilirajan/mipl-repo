# Swagger\Client\SendRawCommandApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**sendRawCommand**](SendRawCommandApi.md#sendRawCommand) | **POST** /command/raw/send/{atoken}/{type} | Send a raw command to a device


# **sendRawCommand**
> \Swagger\Client\Model\CommandStatus[] sendRawCommand($atoken, $type, $command)

Send a raw command to a device

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\SendRawCommandApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in user
$type = "type_example"; // string | data type
$command = new \Swagger\Client\Model\Command(); // \Swagger\Client\Model\Command | Command JSON object

try {
    $result = $apiInstance->sendRawCommand($atoken, $type, $command);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SendRawCommandApi->sendRawCommand: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **type** | **string**| data type |
 **command** | [**\Swagger\Client\Model\Command**](../Model/Command.md)| Command JSON object |

### Return type

[**\Swagger\Client\Model\CommandStatus[]**](../Model/CommandStatus.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

