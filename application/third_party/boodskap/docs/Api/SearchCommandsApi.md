# Swagger\Client\SearchCommandsApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**searchCommands**](SearchCommandsApi.md#searchCommands) | **POST** /command/search/{atoken}/{page}/{pageSize} | Search Commands


# **searchCommands**
> \Swagger\Client\Model\SearchResult searchCommands($atoken, $page, $page_size, $filter)

Search Commands

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\SearchCommandsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in user
$page = 56; // int | Page index
$page_size = 56; // int | Maximum number of entities to be listed, max 250
$filter = new \Swagger\Client\Model\SearchQuery(); // \Swagger\Client\Model\SearchQuery | Optional SearchQuery JSON object, **domainKey** field must be in the search

try {
    $result = $apiInstance->searchCommands($atoken, $page, $page_size, $filter);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling SearchCommandsApi->searchCommands: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **page** | **int**| Page index |
 **page_size** | **int**| Maximum number of entities to be listed, max 250 |
 **filter** | [**\Swagger\Client\Model\SearchQuery**](../Model/SearchQuery.md)| Optional SearchQuery JSON object, **domainKey** field must be in the search | [optional]

### Return type

[**\Swagger\Client\Model\SearchResult**](../Model/SearchResult.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

