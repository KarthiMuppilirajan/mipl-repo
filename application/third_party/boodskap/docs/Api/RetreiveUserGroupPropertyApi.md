# Swagger\Client\RetreiveUserGroupPropertyApi

All URIs are relative to *https://api.boodskap.io*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getUserGroupProperty**](RetreiveUserGroupPropertyApi.md#getUserGroupProperty) | **GET** /user/group/property/get/{atoken}/{ouid}/{gid}/{name} | Retreive UserGroupProperty


# **getUserGroupProperty**
> \Swagger\Client\Model\UserGroupProperty getUserGroupProperty($atoken, $ouid, $gid, $name)

Retreive UserGroupProperty

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');

$apiInstance = new Swagger\Client\Api\RetreiveUserGroupPropertyApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client()
);
$atoken = "atoken_example"; // string | Auth token of the logged in user
$ouid = "ouid_example"; // string | Owner User ID
$gid = 56; // int | User Group ID
$name = "name_example"; // string | 

try {
    $result = $apiInstance->getUserGroupProperty($atoken, $ouid, $gid, $name);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling RetreiveUserGroupPropertyApi->getUserGroupProperty: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **atoken** | **string**| Auth token of the logged in user |
 **ouid** | **string**| Owner User ID |
 **gid** | **int**| User Group ID |
 **name** | **string**|  |

### Return type

[**\Swagger\Client\Model\UserGroupProperty**](../Model/UserGroupProperty.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

