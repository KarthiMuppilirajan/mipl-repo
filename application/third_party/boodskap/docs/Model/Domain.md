# Domain

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**domain_key** | **string** |  | [optional] 
**email** | **string** |  | 
**country** | **string** |  | [optional] 
**state** | **string** |  | [optional] 
**city** | **string** |  | [optional] 
**address** | **string** |  | [optional] 
**zipcode** | **string** |  | [optional] 
**primary_phone** | **string** |  | [optional] 
**locale** | **string** |  | [optional] 
**timezone** | **string** |  | [optional] 
**registered_stamp** | **int** |  | [optional] 
**start_id** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


