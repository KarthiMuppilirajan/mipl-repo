# LookupResult

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data_type** | **string** |  | 
**name** | **string** |  | 
**value** | **string** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


