# CommandStatus

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**device_id** | **string** |  | 
**corr_id** | **int** | unique command identifier or this device | 
**status** | **string** |  | 
**created_stamp** | **int** |  | [optional] 
**queued_stamp** | **int** |  | [optional] 
**sent_stamp** | **int** |  | [optional] 
**acked_stamp** | **int** |  | [optional] 
**command_type** | **string** |  | [optional] 
**data_channel** | **string** |  | [optional] 
**node_id** | **string** |  | [optional] 
**node_uid** | **string** |  | [optional] 
**description** | **string** |  | [optional] 
**reported_ip** | **string** |  | [optional] 
**reported_port** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


