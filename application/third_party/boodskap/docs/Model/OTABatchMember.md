# OTABatchMember

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | 
**device_id** | **string** |  | 
**state** | **string** |  | 
**begin_stamp** | **int** |  | [optional] 
**end_stamp** | **int** |  | [optional] 
**failures** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


