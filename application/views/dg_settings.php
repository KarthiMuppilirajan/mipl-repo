  <!-- Page wrapper  -->
  <div class="page-wrapper"> 
    <!-- Bread crumb -->
    <div class="row page-titles">
      <div class="col-md-5 align-self-center">
        <h3 class="text-primary">Settings</h3>
      </div>
      <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo site_url("dashboard");?>">Home</a></li>
          <li class="breadcrumb-item active">Settings</li>
        </ol>
      </div>
    </div>
    <!-- End Bread crumb --> 
    <!-- Container fluid  -->
	<div class="container-fluid">
         <!-- Start Page Content -->
		<div class="row justify-content">			
			<div class="col-md-12">
			  <div class="card" id="settings-card">
				<div class="card-title">Alert Settings - Genset</div>
				<div class="card-body">
				  <form name="dg-alert" id="alert-settings-dg-form" method="post" action="<?php echo base_url('settings/create_genset'); ?>">
					<?php if($this->session->flashdata('c_dg_success')) {
					?>
					<div class="sufee-alert alert with-close alert-success alert-dismissible fade show">                       
					<?php echo $this->session->flashdata('c_dg_success');  ?>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div> 
					<?php } ?>                 
					
					<?php if($this->session->flashdata('dg_failed')) {
					?>
					<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show" data-dismiss="alert">
					<?php echo $this->session->flashdata('dg_failed');  ?>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">Ã—</span></button>
					</div> 
					<?php } ?>   
					
					<input type="hidden" name="dgalert_id" value="<?=$rkey?>">
					<div class="table-responsive-sm">
					
						<table class="table table-bordered m-b-20">
							<thead>
								<tr>
									<td>Values</th>
									<td>Min</th>
									<td>Max</th>
								</tr>
							</thead>
							<tbody>
								
								<tr>
									<th scope="col">R Phase</th>
									<td><input type="text" class="form-control col-md-8" name="dgrspmin" id="dgrspmin" ></td>
									<td><input type="text" class="form-control col-md-8" name="dgrspmax" id="dgrspmax" ></td>
									
								</tr>
								<tr>
									<th scope="col">B Phase</th>
									<td><input type="text" class="form-control col-md-8" name="dgbspmin" id="dgbspmin" ></td>
									<td><input type="text" class="form-control col-md-8" name="dgbspmax" id="dgbspmax" "></td>
									
								</tr>
								<tr>
									<th scope="col">Y Phase</th>
									<td><input type="text" class="form-control col-md-8" name="dgyspmin" id="dgyspmin" ></td>
									<td><input type="text" class="form-control col-md-8" name="dgyspmax" id="dgyspmax"></td>
									
								</tr>
								<tr>
									<th scope="col">Total Phase</th>
									<td><input type="text" class="form-control col-md-8" name="dgspmin" id="dgspmin" ></td>
									<td><input type="text" class="form-control col-md-8" name="dgspmax" id="dgspmax" ></td>
									
								</tr>
								<tr>
									<th scope="col">R Three Phase</th>
									<td><input type="text" class="form-control col-md-8" name="dgrtpmin" id="dgrtpmin" ></td>
									<td><input type="text" class="form-control col-md-8" name="dgrtpmax" id="dgrtpmax" ></td>
									
								</tr>
								<tr>
									<th scope="col">B Three Phase</th>
									<td><input type="text" class="form-control col-md-8" name="dgbtpmin" id="dgbtpmin" ></td>
									<td><input type="text" class="form-control col-md-8" name="dgbtpmax" id="dgbtpmax" ></td>
									
								</tr>
								<tr>
									<th scope="col">Y Three Phase</th>
									<td><input type="text" class="form-control col-md-8" name="dgytpmin" id="dgytpmin" ></td>
									<td><input type="text" class="form-control col-md-8" name="dgytpmax" id="dgytpmax" ></td>
									
								</tr>
								<tr>
									<th scope="col">Total Three Phase</th>
									<td><input type="text" class="form-control col-md-8" name="dgtpmin" id="dgtpmin" ></td>
									<td><input type="text" class="form-control col-md-8" name="dgtpmax" id="dgtpmax" ></td>
									
								</tr>
								<tr>
									<th scope="col">R Phase Current</th>
									<td><input type="text" class="form-control col-md-8" name="dgrcpmin" id="dgrcpmin" ></td>
									<td><input type="text" class="form-control col-md-8" name="dgrcpmax" id="dgrcpmax" ></td>
									
								</tr>
								<tr>
									<th scope="col">B Phase Current</th>
									<td><input type="text" class="form-control col-md-8" name="dgbcpmin" id="dgbcpmin" ></td>
									<td><input type="text" class="form-control col-md-8" name="dgbcpmax" id="dgbcpmax" ></td>
									
								</tr>
								<tr>
									<th scope="col">Y Phase Current</th>
									<td><input type="text" class="form-control col-md-8" name="dgycpmin" id="dgycpmin" ></td>
									<td><input type="text" class="form-control col-md-8" name="dgycpmax" id="dgycpmax" ></td>
									
								</tr>
								<tr>
									<th scope="col">Total Current</th>
									<td><input type="text" class="form-control col-md-8" name="dgtcpmin" id="dgtcpmin" ></td>
									<td><input type="text" class="form-control col-md-8" name="dgtcpmax" id="dgtcpmax" ></td>
									
								</tr>
								<tr>
									<th scope="col">R Phase Power</th>
									<td><input type="text" class="form-control col-md-8" name="dgrppmin" id="dgrppmin" ></td>
									<td><input type="text" class="form-control col-md-8" name="dgrppmax" id="dgrppmax" ></td>
									
								</tr>
								<tr>
									<th scope="col">B Phase Power</th>
									<td><input type="text" class="form-control col-md-8" name="dgbppmin" id="dgbppmin" ></td>
									<td><input type="text" class="form-control col-md-8" name="dgbppmax" id="dgbppmax" ></td>
									
								</tr>
								<tr>
									<th scope="col">Y Phase Power</th>
									<td><input type="text" class="form-control col-md-8" name="dgyppmin" id="dgyppmin" ></td>
									<td><input type="text" class="form-control col-md-8" name="dgyppmax" id="dgyppmax" ></td>
									
								</tr>
								<tr>
									<th scope="col">Total Power</th>
									<td><input type="text" class="form-control col-md-8" name="dgtppmin" id="dgtppmin" ></td>
									<td><input type="text" class="form-control col-md-8" name="dgtppmax" id="dgtppmax" ></td>
									
								</tr>
								<tr>
									<th scope="col">Frequency</th>
									<td><input type="text" class="form-control col-md-8" name="dgfremin" id="dgfremin" ></td>
									<td><input type="text" class="form-control col-md-8" name="dgfremax" id="dgfremax" ></td>
									
								</tr>
								<tr>
								
									<th scope="col">Power Factor</th>
									<td><input type="text" class="form-control col-md-8" name="dgpfmin" id="dgpfmin" ></td>
									<td><input type="text" class="form-control col-md-8" name="dgpfmax" id="dgpfmax" ></td>
								</tr>
								<tr>
								
									<th scope="col">KVA</th>
									<td><input type="text" class="form-control col-md-8" name="dgkvamin" id="dgkvamin" ></td>
									<td><input type="text" class="form-control col-md-8" name="dgkvamax" id="dgkvamax" ></td>
								</tr>
							</tbody>
							
							
						</table>
						<input type="hidden" name="meter_id" value="<?php echo $this->uri->segment(3);?>">
						<input type="hidden" name="dg_id" value="<?php echo $this->uri->segment(4);?>">
						<button type="submit" class="btn btn-primary">Save</button>
						<button type="reset" class="btn btn-secondary">Reset</button>
				
					</div>        
				  </form>
				</div>
			  </div>
			</div>
		</div>
            
    <!-- End Page wrapper  --> 
</div>
<!-- End Wrapper -->