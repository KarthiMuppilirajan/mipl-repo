<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8"> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
         <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url();?>assets/images/favicon.png">
        <title>Power Management System</title>
         <!-- Bootstrap Core CSS -->
         <link href="<?php echo base_url();?>assets/css/lib/bootstrap/bootstrap.min.css" rel="stylesheet">
         <!-- Custom CSS -->
        <link href="<?php echo base_url();?>assets/css/login.css" rel="stylesheet">
    </head>
    <body class="login">
        <div class="user-login">
            <div class="row">
                <div class="col-sm-12 col-lg-6 login-right d-none d-lg-block">
                    <div id="login-carousel" class="carousel slide" data-ride="carousel">                 
                      <!-- The slideshow -->
                      <div class="carousel-inner">
                        <div class="carousel-item active">  
                            <img src="<?php echo base_url();?>assets/images/login-banner1.png" alt="image">
                           <div class="carousel-caption">
                            <h4>REPORTS</h4>
                            <p>EB Report - Report for EB parameters like Power Factor,Frequency, Single phase voltage, Three Phase voltage, Total Current, Total Power, KVA, and KWH</p>
                          </div> 
                        </div>
                        <div class="carousel-item">         
                            <img src="<?php echo base_url();?>assets/images/login-banner2.png" alt="image">
                           <div class="carousel-caption">                           
                            <h4>WEEKLY CONSUMPTION CHART</h4>
                            <p>Chart representation of main parameters like Frequency, Amp, Single Phase voltage, Three phase voltage and Power </p>
                          </div> 
                        </div>

                        <div class="carousel-item">         
                            <img src="<?php echo base_url();?>assets/images/login-banner3.png" alt="image">
                           <div class="carousel-caption">                           
                            <h4>USER PERMISSIONS</h4>
                            <p>We have 3 user groups - Manager, Supervisor and Technician and can give permission to each user group based on the functional requirements of the group</p>
                          </div> 
                        </div>
                      </div>                  
                    </div>
                </div>
                <div class="col-md-12 col-lg-6 login-left"> 
                    <img class="logo" src="<?php echo base_url();?>assets/images/logo_pms.png" alt="logo"/>
                    <div class="login-page">
                        <div class="login-content"> 
                            <h3 class="title">POWER MANAGEMENT SYSTEM - V1</h3>
                            <h3 class="logintitle">Login</h3>
                             <?php if($this->session->flashdata('invalid_user')) { ?>
                                 <div class="alert alert-danger fade show"> <?php echo $this->session->flashdata('invalid_user');  ?> </div>
                                <?php } ?>
                             <form id="login-form" class="login-form" action="<?php echo base_url('login/loginRequest');?>" method="post">
                                <div class="form-group row">
                                    <div class="col padding-left0">
                                        <input type="email" name="username" class="form-control form-control-solid placeholder-no-fix form-group" placeholder="Email Address" value="<?php if($username) { echo $username; } ?>">
                                    </div>
                                    <div class="col">
                                         <input type="password" name="password" class="form-control form-control-solid placeholder-no-fix form-group"placeholder="Password" value="<?php if($password) { echo $password; } ?>">
                                    </div>
                                </div>
                                <div class=" form-group row">
                                    <div class="col padding-left0">
                                         <label class="remember_me">
                                            <input type="checkbox" name="remember_me" <?php if($username) { echo "checked"; } ?>> Remember me <span></span>
                                        </label>
                                    </div>
                                    <div class="col text-right">
                                        <button class="btn blue" type="submit">Sign In</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="login-footer">
                        <div class="float-left">
                            <div class="copyright">Powered by </div>
                            <a href="http://fourbends.com/" target="_blank"><img class="fb-logo" src="<?php echo base_url();?>assets/images/logo.svg" width="200" alt="logo"/></a>
                        </div>
                        <div class="float-right">
                            <div><i class="fa fa-envelope"></i><a href="mailto:contactus@fourbends.com">contactus@fourbends.com</a></div>
                            <div><i class="fa fa-phone"></i>0452-4522755, 0452-4361117, 9789766755</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            <!-- End Wrapper -->
    <!-- All Jquery -->
    <script src="<?php echo base_url();?>assets/js/lib/jquery/jquery.min.js"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src="<?php echo base_url();?>assets/js/lib/bootstrap/js/popper.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/lib/bootstrap/js/bootstrap.min.js"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src="<?php echo base_url();?>assets/js/jquery.slimscroll.js"></script>
    <!--Menu sidebar -->
    <script src="<?php echo base_url();?>assets/js/sidebarmenu.js"></script>
    <!--stickey kit -->
    <script src="<?php echo base_url();?>assets/js/lib/sticky-kit-master/dist/sticky-kit.min.js"></script>
    
    <script src="<?php echo base_url();?>assets/js/lib/form-validation/jquery.validate.min.js"></script>
    <!--Custom JavaScript -->
    <script src="<?php echo base_url();?>assets/js/login.js"></script>

<?php 
$gaAccount=$this->config->item('gaAccount','fb_boodskap'); 
echo initGA($gaAccount); ?>
</body>
</html>