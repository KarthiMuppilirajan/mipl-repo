  <!-- Page wrapper  -->
  <div class="page-wrapper"> 
    <!-- Bread crumb -->
    <div class="row page-titles">
      <div class="col-md-5 align-self-center">
        <h3 class="text-primary">Settings</h3>
      </div>
      <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo site_url("dashboard");?>">Home</a></li>
          <li class="breadcrumb-item active">Settings</li>
        </ol>
      </div>
    </div>
    <!-- End Bread crumb --> 
    <!-- Container fluid  -->
	<div class="container-fluid">
         <!-- Start Page Content -->
		<div class="row justify-content">			
			<div class="col-md-12">
			  <div class="card" id="settings-card">
				<div class="card-title">Alert Settings - Genset</div>
				<div class="card-body">
				  <form name="dg-alert" id="alert-settings-dg-form" method="post" action="<?php echo base_url('settings/create_genset'); ?>">
					<?php if($this->session->flashdata('c_dg_success')) {
					?>
					<div class="sufee-alert alert with-close alert-success alert-dismissible fade show">                       
					<?php echo $this->session->flashdata('c_dg_success');  ?>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div> 
					<?php } ?>                 
					
					<?php if($this->session->flashdata('dg_failed')) {
					?>
					<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show" data-dismiss="alert">
					<?php echo $this->session->flashdata('dg_failed');  ?>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">Ã—</span></button>
					</div> 
					<?php } ?>   
					
					<input type="hidden" name="dgalert_id" value="<?=$rkey?>">
					<div class="table-responsive-sm">
					
						<table class="table table-bordered m-b-20">
							<thead>
								<tr>
									<td>Values</th>
									<td>Min</th>
									<td>Max</th>
								</tr>
							</thead>
							<tbody>
								
								<tr>
									<th scope="col">R Phase</th>
									<td><input type="text" class="form-control col-md-8" name="dgrspmin" id="dgrspmin" value="<?php 
									if(empty($source['dgrspmin']))
									echo "0";
									else
									echo $source['dgrspmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgrspmax" id="dgrspmax" value="<?php 
									if(empty($source['dgrspmax']))
									echo "0";
									else
									echo $source['dgrspmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">B Phase</th>
									<td><input type="text" class="form-control col-md-8" name="dgbspmin" id="dgbspmin" value="<?php  
									if(empty($source['dgbspmin']))
									echo "0";
									else
									echo $source['dgbspmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgbspmax" id="dgbspmax" value="<?php 
									if(empty($source['dgbspmax']))
									echo "0";
									else
									echo $source['dgbspmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Y Phase</th>
									<td><input type="text" class="form-control col-md-8" name="dgyspmin" id="dgyspmin" value="<?php 
									if(empty($source['dgyspmin']))
									echo "0";
									else
									echo $source['dgyspmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgyspmax" id="dgyspmax" value="<?php 
									if(empty($source['dgyspmax']))
									echo "0";
									else
									echo $source['dgyspmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Total Phase</th>
									<td><input type="text" class="form-control col-md-8" name="dgspmin" id="dgspmin" value="<?php 
									if(empty($source['dgspmin']))
									echo "0";
									else
									echo $source['dgspmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgspmax" id="dgspmax" value="<?php echo $source['dgspmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">R Three Phase</th>
									<td><input type="text" class="form-control col-md-8" name="dgrtpmin" id="dgrtpmin" value="<?php 
									if(empty($source['dgrtpmin']))
									echo "0";
									else
									echo $source['dgrtpmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgrtpmax" id="dgrtpmax" value="<?php echo $source['dgrtpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">B Three Phase</th>
									<td><input type="text" class="form-control col-md-8" name="dgbtpmin" id="dgbtpmin" value="<?php 
									if(empty($source['dgbtpmin']))
									echo "0";
									else
									echo $source['dgbtpmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgbtpmax" id="dgbtpmax" value="<?php echo $source['dgbtpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Y Three Phase</th>
									<td><input type="text" class="form-control col-md-8" name="dgytpmin" id="dgytpmin" value="<?php 
									if(empty($source['dgytpmin']))
									echo "0";
									else
									echo $source['dgytpmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgytpmax" id="dgytpmax" value="<?php 
									if(empty($source['dgytpmax']))
									echo "0";
									else
									echo $source['dgytpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Total Three Phase</th>
									<td><input type="text" class="form-control col-md-8" name="dgtpmin" id="dgtpmin" value="<?php 
									if(empty($source['dgtpmin']))
									echo "0";
									else
									echo $source['dgtpmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgtpmax" id="dgtpmax" value="<?php 
									if(empty($source['dgtpmax']))
									echo "0";
									else
									echo $source['dgtpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">R Phase Current</th>
									<td><input type="text" class="form-control col-md-8" name="dgrcpmin" id="dgrcpmin" value="<?php 
									if(empty($source['dgrcpmin']))
									echo "0";
									else
									echo $source['dgrcpmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgrcpmax" id="dgrcpmax" value="<?php 
									if(empty($source['dgrcpmax']))
									echo "0";
									else
									echo $source['dgrcpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">B Phase Current</th>
									<td><input type="text" class="form-control col-md-8" name="dgbcpmin" id="dgbcpmin" value="<?php 
									if(empty($source['dgbcpmin']))
									echo "0";
									else
									echo $source['dgbcpmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgbcpmax" id="dgbcpmax" value="<?php 
									if(empty($source['dgbcpmax']))
									echo "0";
									else
									echo $source['dgbcpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Y Phase Current</th>
									<td><input type="text" class="form-control col-md-8" name="dgycpmin" id="dgycpmin" value="<?php 
									if(empty($source['dgycpmin']))
									echo "0";
									else
									echo $source['dgycpmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgycpmax" id="dgycpmax" value="<?php 
									if(empty($source['dgycpmax']))
									echo "0";
									else
									echo $source['dgycpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Total Current</th>
									<td><input type="text" class="form-control col-md-8" name="dgtcpmin" id="dgtcpmin" value="<?php 
									if(empty($source['dgtcpmin']))
									echo "0";
									else
									echo $source['dgtcpmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgtcpmax" id="dgtcpmax" value="<?php 
									if(empty($source['dgtcpmax']))
									echo "0";
									else
									echo $source['dgtcpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">R Phase Power</th>
									<td><input type="text" class="form-control col-md-8" name="dgrppmin" id="dgrppmin" value="<?php 
									if(empty($source['dgrppmin']))
									echo "0";
									else
									echo $source['dgrppmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgrppmax" id="dgrppmax" value="<?php 
									if(empty($source['dgrppmax']))
									echo "0";
									else
									echo $source['dgrppmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">B Phase Power</th>
									<td><input type="text" class="form-control col-md-8" name="dgbppmin" id="dgbppmin" value="<?php 
									if(empty($source['dgbppmin']))
									echo "0";
									else
									echo $source['dgbppmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgbppmax" id="dgbppmax" value="<?php 
									if(empty($source['dgbppmax']))
									echo "0";
									else
									echo $source['dgbppmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Y Phase Power</th>
									<td><input type="text" class="form-control col-md-8" name="dgyppmin" id="dgyppmin" value="<?php 
									if(empty($source['dgyppmin']))
									echo "0";
									else
									echo $source['dgyppmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgyppmax" id="dgyppmax" value="<?php 
									if(empty($source['dgyppmax']))
									echo "0";
									else
									echo $source['dgyppmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Total Power</th>
									<td><input type="text" class="form-control col-md-8" name="dgtppmin" id="dgtppmin" value="<?php 
									if(empty($source['dgtppmin']))
									echo "0";
									else
									echo $source['dgtppmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgtppmax" id="dgtppmax" value="<?php 
									if(empty($source['dgtppmax']))
									echo "0";
									else
									echo $source['dgtppmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Frequency</th>
									<td><input type="text" class="form-control col-md-8" name="dgfremin" id="dgfremin" value="<?php 
									if(empty($source['dgfremin']))
									echo "0";
									else
									echo $source['dgfremin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgfremax" id="dgfremax" value="<?php 
									if(empty($source['dgfremax']))
									echo "0";
									else
									echo $source['dgfremax']; ?>"></td>
									
								</tr>
								<tr>
								
									<th scope="col">Power Factor</th>
									<td><input type="text" class="form-control col-md-8" name="dgpfmin" id="dgpfmin" value="<?php 
									if(empty($source['dgpfmin']))
									echo "0";
									else
									echo $source['dgpfmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgpfmax" id="dgpfmax" value="<?php 
									if(empty($source['dgpfmax']))
									echo "0";
									else
									echo $source['dgpfmax']; ?>"></td>
								</tr>
								<tr>
								
									<th scope="col">KVA</th>
									<td><input type="text" class="form-control col-md-8" name="dgkvamin" id="dgkvamin" value="<?php 
									if(empty($source['dgkvamin']))
									echo "0";
									else
									echo $source['dgkvamin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="dgkvamax" id="dgkvamax" value="<?php 
									if(empty($source['dgkvamax']))
									echo "0";
									else
									echo $source['dgkvamax']; ?>"></td>
								</tr>
							</tbody>
							
						</table>
						<input type="hidden" name="meter_id" value="<?php echo $this->uri->segment(3);?>">
						<input type="hidden" name="dg_id" value="<?php echo $this->uri->segment(4);?>">
						<button type="submit" class="btn btn-primary">Save</button>
						<button type="reset" class="btn btn-secondary">Reset</button>
				
					</div>        
				  </form>
				</div>
			  </div>
			</div>
		</div>
            
    <!-- End Page wrapper  --> 
</div>
<!-- End Wrapper -->