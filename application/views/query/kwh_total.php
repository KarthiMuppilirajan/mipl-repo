{
  "query": {
    "bool": {
      "must": [
       <?php if(!empty($min_date) && !empty($max_date) ): ?>
        {
          "range": {
            "__orderfld__": {
              "gte": "__min_date__",
              "lte": "__max_date__",
              "boost": 2.0
            }
          }
        },
        <?php endif; ?>
        {
          "match": {
            "device_id": __meter_id__
          }
        }
      ]
    }
  },
  "size" : 1,
  "from": 0
}