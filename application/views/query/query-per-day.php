{"size" : 0,
 "query": {
        "range" : {
            "createdtime" : {
                "gte" : "now-1d/d",
                "lt" :  "now/d"
            }
        }
    }, "aggs" : {
        "max_time" : { "max" : { "field" : "createdtime" } },
		"min_time" : { "min" : { "field" : "createdtime" } }
    }
}