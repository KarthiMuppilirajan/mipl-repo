{ 
	"size" : 0,
	  "query": {
	    "bool": {
	      "must": [
	      <?php if(!empty($min_date) && !empty($max_date) ): ?>
	        {
	          "range": {
	            "__orderfld__": {
	              "gte": "__min_date__",
	              "lte": "__max_date__",
	              "boost": 2.0
	            }
	          }
	        },
	      <?php endif; ?>	      
	        {
	          "match": {
	            "gen": __val__
	          }
	        },
	        {
	          "match": {
	            "meter_id": __meter_id__
	          }
	        }
	      ]
	    }
	  },
	<?php if(!empty($hr) ): ?>	  
    "aggregations" : {
        "runtime" : {
            "date_histogram" : {
                "field" : "createdtime",
                "interval" : "<?php echo $hr;?>H",
                "min_doc_count": 1
            },"aggs": {
				"tops": {
				  "top_hits": {
					"size": 1
				  }
				}
			}
        }
    },
    <?php endif; ?>	
    <?php if(empty($hr) ): ?>
	  "size": __size__,
	  "from": __from__,
	 <?php endif; ?>	
	  "sort": {
	    "__orderfld__": {
	      "order": "__orderdir__"
	    }
	  } 	    
}