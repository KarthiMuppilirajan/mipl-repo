{ 
	"size" : 0,
	  "query": {
	    "bool": {
	      "must": [
	        {
	          "match": {
	            "gen": __val__
	          }
	        },
	        {
	          "match": {
	            "meter_id": __meter_id__
	          }
	        }
	      ]
	    }
	  },
    "aggregations" : {
        "runtime" : {
            "date_histogram" : {
                "field" : "createdtime",
                "interval" : "1H"
            },"aggs": {
				"tops": {
				  "top_hits": {
					"size": 1
				  }
				}
			}
        }
    }
}