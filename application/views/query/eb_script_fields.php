{
"size":0,
 "query": { "match_all" : {}   },
	"aggs": {
		
		"runtime": {
			"terms": {
				
				"script": "doc.status_on.date.secondOfDay - doc.status_off.date.secondOfDay"
			},
			"aggs": {
				"tops": {
				  "top_hits": {
					"size": 100000
				  }
				}
			}
		}
		
	}
}