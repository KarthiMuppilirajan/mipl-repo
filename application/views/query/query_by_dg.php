{
  "query": {
    "bool": {
      "must": [
        {
          "match": {
            "dg_id": __dg_id__
          }
        },
        {
          "match": {
            "meter_id": __meter_id__
          }
        }
      ]
    }
  }
},
  "size" : __size__,
  "from": __from__,
  "sort": { "__orderfld__" : {"order" : "__orderdir__"} }
}