{
  "query": {
    "bool": {
      "must": [
        {
          "range": {
            "__range_fld__": {
              "gte": "__lowv__",
              "lte": "__topv__",
              "boost": 2.0
            }
          }
        },
        {
          "match": {
            "meter_id": __meter_id__
          }
        }
      ]
    }
  },
  "size": __size__,
  "from": __from__,
  "sort": {
    "__orderfld__": {
      "order": "__orderdir__"
    }
  }
}