{
  "query": {
    "bool": {
      "must": [   
      <?php if($calltype=="reports" ): ?>  
        {
          "range": {
            "createdtime": {
              "gte":__time__,
              "boost": 2.0
            }
          }
        }, 
        {
          "match": {
            "gen": "__val__"
          }
        },
        {
          "match": {
            "meter_id": __meter_id__
          }
        }        
        <?php else: ?>
        {
          "range": {
            "last_on_time": {
              "gte":__time__,
              "boost": 2.0
            }
          }
        },
        {
          "match": {
            "device_id": __meter_id__
          }
        }        
        <?php endif; ?>
      ]
    }
  },
  "size" : 1,
  "from": 0
}