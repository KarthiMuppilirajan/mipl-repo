{
  "query": {
    "bool": {
      "must": [
        {
          "range": {
            "last_on_time": {
              "gte": "__min_date__",
              "lte": "__max_date__",
              "boost": 2
            }
          }
        },
         {
          "match": {
            "device_id": __meter_id__
          }
        },
        {
          "match": {
            "dg_id": __dg_id__
          }
        }
      ]
    }
  },
  "aggs": {
    "runtime": {
      "terms": {
        "script": "doc.last_off_time.date.secondOfDay - doc.last_on_time.date.secondOfDay"
      },
      "aggs": {
        "tops": {
          "top_hits": {
            "size": 100000
          }
        }
      }
    }
  }
}