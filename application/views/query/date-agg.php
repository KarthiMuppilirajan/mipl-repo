{
	"query": { "match":{ "createdtime" : "now-10h" } },
    "aggs" : {
		"sales_over_time" : {
            "date_histogram" : {
                "field" : "createdtime",
                "interval" : "1d",
                "format" : "yyyy-MM-dd" 
            }
        },
        "red_products" : {
            "filter" : { "term": { "gen": "1" } },
            "aggs" : {
                "avg_price" : { "avg" : { "field" : "createdtime" } }
            }
        }
    }
}