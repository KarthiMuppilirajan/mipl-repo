{
 "query": {
        "constant_score" : {
            "filter" : {
                 "bool" : {
                    "must" : [
                        { "term" : { "meter_id" : "__meter_id__" } },
                        { "term" : { "gen" : "0" } }
                    ]
                }
            }
        }
    },
	"size" : 1,
	"from": 0
}