{
 "query": {
        "constant_score" : {
            "filter" : {
                 "bool" : {
                    "must" : [
                        { "term" : { "meter_id" : "__meter_id__" } },
                        { "term" : { "gen" : "1" } },
                        { "term" : { "dg_id" : "__dg_id__" } }
                    ]
                }
            }
        }
    },
	"size" : 1,
	"from": 0
}