  <!-- header header  -->
  <?php $this->load->view('include/header_view');	?>
  <!-- End header header --> 
  <!-- Left Sidebar  -->
  <?php $this->load->view('include/left-sidebar');	?>
  <!-- End Left Sidebar  --> 
  <!-- Page wrapper  -->
  <div class="page-wrapper"> 
    <!-- Bread crumb -->
    <div class="row page-titles">
      <div class="col-md-5 align-self-center">
        <h3 class="text-primary">Settings</h3>
      </div>
      <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo site_url("dashboard");?>">Home</a></li>
          <li class="breadcrumb-item active">Settings</li>
        </ol>
      </div>
    </div>
    <!-- End Bread crumb --> 
    <!-- Container fluid  -->
	<div class="container-fluid">
                <!-- Start Page Content -->
		<div class="row justify-content">
		
			<div class="col-md-12">
			  <div class="card" id="alertstatus-card">
				<div class="card-title">Alert Status</div>
				<div class="card-body">
				  
					<?php if($this->session->flashdata('alert_status_update_success')) {
					?>
					<div class="sufee-alert alert with-close alert-success alert-dismissible fade show"> <?php echo $this->session->flashdata('alert_status_update_success');  ?>
					  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
					</div>
					<?php } ?>
					<?php if($this->session->flashdata('alert_status_update_failed')) { ?>
					<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show" data-dismiss="alert"> <?php echo $this->session->flashdata('alert_status_update_failed');  ?>
					  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
					</div>
					<?php } ?>  
					
					
					<?php if(1): ?>
					
					<div class="table-responsive-sm">
					
						<table class="table table-bordered" role="grid" id="settings-table">
							<thead>
								<tr>
									<td>Alert name</td>
									<td>Actions</td>
								</tr>
							</thead>
							 
							<tbody>
								<?php 
								//print_r($alert_status_result_set);
								foreach($alert_status_result_set as $key=>$row): 
									  $source = $row["_source"];
									  $rkey = $row["_id"];
								   ?>
								<tr>
								
									<th><?php echo $source["alert_name"]; ?></th>									
									<td>
									
										<?php if(1): ?>
										<?php 
										if($source["alert_flag"]=="true")
										{ 
										?>
										<a href="<?php echo base_url().'settings/updateAlertflag/'.$rkey.'/false';?>" title="De-activate" data-id="<?php echo $rkey; ?>"><i 
                            class="fa fa-eye"></i></a>&nbsp;
										<?php } else { ?>
										<a href="<?php echo base_url().'settings/updateAlertflag/'.$rkey.'/true';?>" title="Activate" data-id="<?php echo $rkey; ?>">
                            <i class="fa fa-eye-slash"></i></a>&nbsp;
										<?php } ?> 
										<?php endif; ?>
				
									
									</td>
								
								</tr>
								<?php endforeach; ?>
							</tbody>
							
								
							
						</table>
					</div>        
				    
					<?php endif; ?>
				  
				</div>
			  </div>
			</div>
		
			<div class="col-md-12">
			  <div class="card" id="settings-card">
				<div class="card-title">Alert Notification Settings</div>
				<div class="card-body">
				  
				    <?php if( has_accessable('add_alert_settings') ): ?>
					<button type="button" class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#notify-card">Add New</button>
					<?php endif; ?>
					
					<?php if($this->session->flashdata('a_success')) {
					?>
					<div class="sufee-alert alert with-close alert-success alert-dismissible fade show">                       
					<?php echo $this->session->flashdata('a_success');  ?>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
					</div> 
					<?php } ?>                 
					<?php if($this->session->flashdata('update_success')) {
					?>
					<div class="sufee-alert alert with-close alert-success alert-dismissible fade show"> <?php echo $this->session->flashdata('update_success');  ?>
					  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
					</div>
					<?php } ?>
					<?php if($this->session->flashdata('update_failed')) { ?>
					<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show" data-dismiss="alert"> <?php echo $this->session->flashdata('update_failed');  ?>
					  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
					</div>
					<?php } ?>  
					<?php if($this->session->flashdata('failed')) {
					?>
					<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show" data-dismiss="alert">
					<?php echo $this->session->flashdata('failed');  ?>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
					</div> 
					<?php } ?>   
					
					<?php if( has_accessable('list_alert_settings') ): ?>
					
					<div class="table-responsive-sm">
					
						<table class="table table-bordered" role="grid" id="settings-table">
							<thead>
								<tr>
									<td>Name</td>
									<td>Phone No.</td>
									<td>Email</td>
									<td>Actions</td>
								</tr>
							</thead>
							 
							<tbody>
								<?php foreach($result_set1 as $key=>$row): 
									  $source = $row["_source"];
									  $rkey = $row["_id"];
									  
									 
								   ?>
								<tr>
								
									<th><?php echo $source["name"]; ?></th>
									<td><?php echo $source["ph_no"]; ?></td>
									<td><?php echo $source["email"]; ?></td>
									<td>
									
									<a href="javascript:void(0);" data-id="<?=$rkey?>" title="Edit" class="edit-notify" ><i class="fa fa-edit"></i></a>&nbsp;&nbsp;
									<a href="#" data-id="<?=$rkey?>" class="delete-modal" title="Delete"data-toggle="modal" data-target="#delete-modal"><i class="fa fa-trash"></i></a>&nbsp;&nbsp;
								
									</td>
								
								</tr>
								<?php endforeach; ?>
							</tbody>
							
								
							
						</table>
					</div>        
				    
					<?php endif; ?>
				  
				</div>
			  </div>
			</div>

		</div>
                <!-- End PAge Content -->
    </div>
            
    <!-- End Page wrapper  --> 
</div>
<!-- End Wrapper -->

<div class="modal" id="notify-card" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
		<form name="notify" id="notify-form" method="post" action="<?php echo base_url('settings/add_record');?>">
			
				<div class="modal-header">
					<h5 class="modal-title" id="deviceLabel"><b>Add New</b></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
					
				</div>
				<div class="modal-body">
				<div class="modal-loader">
					<div class="form-group">
					<div class="form-group col-md-12">
					<label>Name:</label>
						<input type="text" id="name" name="name"  class="form-control" placeholder="Name"/>
					</div>
					</div>
					<div class="form-group">
					<div class="form-group col-md-12">
					<label>Phone Number:</label>
						(Ex: 987654321)
						<input type="text" id="ph_no" name="ph_no" class="form-control" placeholder="Phone Number"/>
					</div>
					</div>
					<div class="form-group">
					<div class="form-group col-md-12">
					<label>Email:</label>
					<input type="text" id="email" name="email" class="form-control" placeholder="Email Id"/>
				  </div>
					</div>
					
				</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Add Member</button>
					<button type="reset" class="btn btn-secondary clear">Clear</button>
				</div>
				
			</form>
		</div>
	</div>
</div>


<div class="modal" id="delete-modal" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="staticModalLabel">Delete</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <p>
               Are You Sure Want to Delete?
            </p>
        </div>
        <div class="modal-footer">
        	<form method="post"  action="<?php echo base_url('settings/delete');?>">
            <input type="hidden" name="rid" id="delete_rid"/>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary">Confirm</button>
            </form>
        </div>
    </div>
</div>
</div>


<?php $this->load->view('include/footer');	?>
