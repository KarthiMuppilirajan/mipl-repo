  <!-- Page wrapper  -->
  <div class="page-wrapper"> 
    <!-- Bread crumb -->
    <div class="row page-titles">
      <div class="col-md-5 align-self-center">
        <h3 class="text-primary">Settings</h3>
      </div>
      <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo site_url("dashboard");?>">Home</a></li>
          <li class="breadcrumb-item active">Settings</li>
        </ol>
      </div>
    </div>
    <!-- End Bread crumb --> 
    <!-- Container fluid  -->
	<div class="container-fluid">
         <!-- Start Page Content -->
		<div class="row justify-content">			
			<div class="col-md-12">
			  <div class="card" id="settings-card">
				<div class="card-title">Alert Settings - EB </div>
				<div class="card-body">
				  <form name="alert" id="alert-settings-form" method="post" action="<?php echo base_url('settings/create'); ?>">

					<input type="hidden" name="ebalert_id" value="<?=$rkey?>">
					<div class="table-responsive-sm">
						<table class="table table-bordered m-b-20">
							<thead>
								<tr>
									<td>Values</th>
									<td>Min</th>
									<td>Max</th>
								</tr>
							</thead>
							<tbody>
								
								<tr>
									<th scope="col">R Phase</th>
									<td><input type="text" class="form-control col-md-8" name="rspmin" id="rspmin" value=""></td>
									<td><input type="text" class="form-control col-md-8" name="rspmax" id="rspmax" value=""></td>
									
								</tr>
								<tr>
									<th scope="col">B Phase</th>
									<td><input type="text" class="form-control col-md-8" name="bspmin" id="bspmin" value=""></td>
									<td><input type="text" class="form-control col-md-8" name="bspmax" id="bspmax" value=""></td>
									
								</tr>
								<tr>
									<th scope="col">Y Phase</th>
									<td><input type="text" class="form-control col-md-8" name="yspmin" id="yspmin" value=""></td>
									<td><input type="text" class="form-control col-md-8" name="yspmax" id="yspmax" value=""></td>
									
								</tr>
								<tr>
									<th scope="col">Total Phase</th>
									<td><input type="text" class="form-control col-md-8" name="spmin" id="spmin" value=""></td>
									<td><input type="text" class="form-control col-md-8" name="spmax" id="spmax" value=""></td>
									
								</tr>
								<tr>
									<th scope="col">R Three Phase</th>
									<td><input type="text" class="form-control col-md-8" name="rtpmin" id="rtpmin" value=""></td>
									<td><input type="text" class="form-control col-md-8" name="rtpmax" id="rtpmax" value=""></td>
									
								</tr>
								<tr>
									<th scope="col">B Three Phase</th>
									<td><input type="text" class="form-control col-md-8" name="btpmin" id="btpmin" value=""></td>
									<td><input type="text" class="form-control col-md-8" name="btpmax" id="btpmax" value=""></td>
									
								</tr>
								<tr>
									<th scope="col">Y Three Phase</th>
									<td><input type="text" class="form-control col-md-8" name="ytpmin" id="ytpmin" value=""></td>
									<td><input type="text" class="form-control col-md-8" name="ytpmax" id="ytpmax" value=""></td>
									
								</tr>
								<tr>
									<th scope="col">Total Three Phase</th>
									<td><input type="text" class="form-control col-md-8" name="tpmin" id="tpmin" value=""></td>
									<td><input type="text" class="form-control col-md-8" name="tpmax" id="tpmax" value=""></td>
									
								</tr>
								<tr>
									<th scope="col">R Phase Current</th>
									<td><input type="text" class="form-control col-md-8" name="rcpmin" id="rcpmin" value=""></td>
									<td><input type="text" class="form-control col-md-8" name="rcpmax" id="rcpmax" value=""></td>
									
								</tr>
								<tr>
									<th scope="col">B Phase Current</th>
									<td><input type="text" class="form-control col-md-8" name="bcpmin" id="bcpmin" value=""></td>
									<td><input type="text" class="form-control col-md-8" name="bcpmax" id="bcpmax" value=""></td>
									
								</tr>
								<tr>
									<th scope="col">Y Phase Current</th>
									<td><input type="text" class="form-control col-md-8" name="ycpmin" id="ycpmin" value=""></td>
									<td><input type="text" class="form-control col-md-8" name="ycpmax" id="ycpmax" value=""></td>
									
								</tr>
								<tr>
									<th scope="col">Total Current</th>
									<td><input type="text" class="form-control col-md-8" name="tcpmin" id="tcpmin" value=""></td>
									<td><input type="text" class="form-control col-md-8" name="tcpmax" id="tcpmax" value=""></td>
									
								</tr>
								<tr>
									<th scope="col">R Phase Power</th>
									<td><input type="text" class="form-control col-md-8" name="rppmin" id="rppmin" value=""></td>
									<td><input type="text" class="form-control col-md-8" name="rppmax" id="rppmax" value=""></td>
									
								</tr>
								<tr>
									<th scope="col">B Phase Power</th>
									<td><input type="text" class="form-control col-md-8" name="bppmin" id="bppmin" value=""></td>
									<td><input type="text" class="form-control col-md-8" name="bppmax" id="bppmax" value=""></td>
									
								</tr>
								<tr>
									<th scope="col">Y Phase Power</th>
									<td><input type="text" class="form-control col-md-8" name="yppmin" id="yppmin" value=""></td>
									<td><input type="text" class="form-control col-md-8" name="yppmax" id="yppmax" value=""></td>
									
								</tr>
								<tr>
									<th scope="col">Total Power</th>
									<td><input type="text" class="form-control col-md-8" name="tppmin" id="tppmin" value=""></td>
									<td><input type="text" class="form-control col-md-8" name="tppmax" id="tppmax" value=""></td>
									
								</tr>
								<tr>
									<th scope="col">Frequency</th>
									<td><input type="text" class="form-control col-md-8" name="fremin" id="fremin" value=""></td>
									<td><input type="text" class="form-control col-md-8" name="fremax" id="fremax" value=""></td>
									
								</tr>
								<tr>
								
									<th scope="col">Power Factor</th>
									<td><input type="text" class="form-control col-md-8" name="pfmin" id="pfmin" value=""></td>
									<td><input type="text" class="form-control col-md-8" name="pfmax" id="pfmax" value=""></td>
								</tr>
								<tr>
								
									<th scope="col">KVA</th>
									<td><input type="text" class="form-control col-md-8" name="kvamin" id="kvamin" value=""></td>
									<td><input type="text" class="form-control col-md-8" name="kvamax" id="kvamax" value=""></td>
								</tr>								
							</tbody>
							
						</table>
						<input type="hidden" name="meter_id" value="<?php echo $this->uri->segment(3);?>">
						<button type="submit" class="btn btn-primary">Save</button>
						<button class="btn btn-secondary reset-btn">Reset</button>
				
					</div>        
				  </form>
				</div>
			  </div>
			</div>
	</div>
            
    <!-- End Page wrapper  --> 
</div>
<!-- End Wrapper -->