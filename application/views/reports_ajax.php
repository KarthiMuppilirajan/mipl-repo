  <!-- Page wrapper  -->
  <input type="hidden" name="meter_id" id="main_meter_id" value="<?=$meter_id;?>">
  <div class="page-wrapper"> 
    <!-- Bread crumb -->
    <div class="row page-titles">
      <div class="col-md-5 align-self-center">
        <h3 class="text-primary"><?php echo $title; ?></h3>
      </div>
      <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
          <li class="breadcrumb-item active"><?php echo $title; ?></li>
        </ol>
      </div>
    </div>
    <!-- End Bread crumb --> 
    <!-- Container fluid  -->
    <div class="container-fluid"> 
      <!-- Start Page Content -->
      <div class="row">
        <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Data Export</h4>
                                <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                                <div class="row">
                                	<form name="filter-form" id="filter-form" action="#" class="">
                                	<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<label class="control-label">From Date</label>
											<input type="text" id="min-date" name="min_date" class="form-control date-picker calendar date-range-min " />
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label class="control-label">To Date</label>
											<input type="text" id="max-date" name="max_date" class="form-control date-picker calendar date-range-max"/>
										</div>
									</div> 
									<div class="col-md-4">
										<div class="form-group">
											
											<input  type="submit" id="submit-filter" class="btn btn-primary m-t-30" value="Submit">
										</div>
									</div>									
								<!--<div class="col-md-3">
										<div class="form-group">
											<label class="control-label">No of records</label>
											<input type="text" id="numof-records" name="numof_records" class="form-control"/>
										</div>
								</div> 	
									<div class="col-md-3">
										<div class="form-group">
											
											<input  type="submit" id="submit-filter" class="btn btn-primary m-t-30" value="Submit">
										</div>
									</div> -->
								</div>
								<input type="hidden" name="meter_id" value="<?=$meter_id?>">
								<?php if($title=="EB Reports"){?>
								<input type="hidden" name="filter-type" value="0">
								<?php } else{?>
								<input type="hidden" name="filter-type" value="1">
								<?php } ?>
								</form>										
								<div class="table-responsive m-t-40" id="table-responsive-report"> 

                                </div>
                            </div>
                        </div>
        </div>
      
      
      <!-- End PAge Content --> 
    </div>
    <!-- End Container fluid  --> 
    <!-- footer --> 
    
    <!-- End footer --> 
  </div>
  <!-- End Page wrapper  --> 
</div>
<!-- End Wrapper -->
</div>

<script id="report-template" type="text/x-handlebars-template">
								
    <table id="report-table" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
		<thead>
			<tr>
				<td>Date - Time</td>
				<td>Power Factor </td>
				<td>Frequency</td>
				<td>Total single Phase Voltage</td>
				<td>Total Three Phase Voltage</td>
				<td>TOTAL Current </td>
				<td>Total POWER (KW) </td>
				<td>Kilowatt-hours</td>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td>Date - Time</td>
				<td>Power Factor </td>
				<td>Frequency</td>
				<td>Total single Phase Voltage</td>
				<td>Total Three Phase Voltage</td>
				<td>TOTAL Current </td>
				<td>Total POWER (KW) </td>
				<td>Kilowatt-hours</td>
			</tr>
			</tfoot>
		        <tbody>
		        	{{#objects}}
					<tr>
						<td>{{createdtime}}</td>
						<td>{{formatFloat pf}}</td>
						<td>{{formatFloat fre}}</td>
						<td>{{formatFloat tv}}</td>
						<td>{{formatFloat tpv}}</td>
						<td>{{formatFloat tc}}</td>
						<td>{{formatFloat tp}}</td>
						<td>{{formatFloat kwh}}</td>
					</tr>		            
		            {{/objects}}
		        </tbody>
		    </table>
</script>
