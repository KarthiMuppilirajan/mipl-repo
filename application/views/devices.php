<div class="page-wrapper">
            <!-- Bread crumb -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h3 class="text-primary">Devices</h3> </div>
                <div class="col-md-7 align-self-center">
                   							
                    <ol class="breadcrumb">
                        <li> <button type="button" class="btn btn-primary btn-sm m-r-20" data-toggle="modal" data-target="#device">Add Device</button></li>
                        <li class="breadcrumb-item"><a href="<?php echo site_url("/dashboard");?>">Home</a></li>
                        <li class="breadcrumb-item active">Devices</li>
                    </ol>
                </div>
            </div>
            <!-- End Bread crumb -->
			<!-- Container fluid  -->
            <div class="container-fluid">
                <!-- Start Page Content -->
				<div class="row" id="deviceform-card">
                        <?php 

                        foreach($result_set as $row): 
                          $source = $row["_source"];
                          $rkey = $row["_id"];

          
                          $watts = $source["watts"];
                          $device_type = $source["device_type"];
                          $d_id = $source["device_id"]; 
                          $state = $source["device_status"];
                          $CI =& get_instance();
                          $gensetStatus= $CI->deviceinGenset($rkey);

                            $CI->config->load('fb_boodskap', TRUE);
                            $devicesArr = $CI->config->item('devicesArr', 'fb_boodskap');
                            $iconImg= $devicesArr[$device_type]['icon']; 
                       ?>                    
				        <div class="col-md-3">
                          <div class="card">
                            <div class="card-title">
                              <?php echo $source["device_id"].": ".$source["device_name"]; ?>
                                
                            </div>
                            <div class="card-content text-center">
                            <div class="bulb">
                                <?php if($state=="true") { ?>
                                <img class="active_device" alt="<?php $source["device_name"]; ?>" src="<?php 
                                $iconImg = str_replace("idle", "on", $iconImg);
                                echo base_url()."assets/images/icons/".$iconImg;?>">
                                <?php } else{?>
                              <img class="active_device" alt="<?php $source["device_name"]; ?>" src="<?php echo base_url()."assets/images/icons/".$iconImg;?>">
                                <?php } ?>
                              <div class="bulb__consumption"><?php echo round($watts,2); ?> W</div>
                            <?php
                                    if($state == "true"){
                                        echo "<label class='switch'>";
                                        echo "<input type='checkbox' data-id='$rkey' data-device_id='$d_id' class='status-switch' checked>";
                                        echo "<span class='slider round'></span>";
                                        echo "</label>";
                                    }else{
                                        echo "<label class='switch'>";
                                        echo "<input type='checkbox' data-id='$rkey' data-device_id='$d_id' class='status-switch'>";
                                        echo "<span class='slider round'></span>";
                                        echo "</label>";
                                    }
                                  ?> 
                                
                            </div>              
                            </div>
                              <div class="button-list">
                                  <a href="<?php echo base_url()."devices/details/".$d_id?>" class="btn btn-info btn-xs m-b-10 m-l-5">Details</a>
                                  <?php if($gensetStatus==false){ ?>
                                  <button data-id="<?= $rkey?>" data-name="<?=$source["device_name"]; ?>" data-watts="<?=$watts; ?>" data-type="<?= $device_type; ?>" class="btn btn-danger btn-xs m-b-10 m-l-5 pull-right addto-genset">Add to genset</button>
                                  <?php } ?>
                              </div>
                          </div>
                        </div>
                    <?php endforeach; ?>                    
                    <!-- /# column -->
				</div>
                <!-- /# row -->
			</div>
		</div>	
<!--device Form-->
<div class="modal" id="device" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
		<form name="device" id="devices-form" method="post" action="<?php echo base_url('devices/add_record');?>">
			
				<div class="modal-header">
					<h5 class="modal-title" id="deviceLabel"><b>Add Device</b></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">&times;</button>
					
				</div>
				<div class="modal-body">
				<div class="modal-loader">
					<div class="form-group">
					<div class="form-group col-md-12">
					<label>Device Id:</label>
						<input type="text" id="idmodel" name="device_id"  class="form-control" placeholder="first letter of room and unique number"/>
					</div>
					</div>
					<div class="form-group">
					<div class="form-group col-md-12">
					<label>Device Name:</label>
						<input type="text" id="namemodel" name="device_name" class="form-control" placeholder="Name of the Device"/>
					</div>
					</div>
          <div class="form-group">
          <div class="form-group col-md-12">
          <label>Watts:</label>
            <input type="text" id="watts" name="watts" class="form-control" placeholder="Watts"/>
          </div>
          </div>          
          <div class="form-group">
          <div class="form-group col-md-12">
          <label>Device Type:</label>          
          <select name="device_type" id="device_type" class="form-control">
                      <option selected value="">Choose...</option>
                      <?php 
                            $ci =& get_instance();
                            $ci->config->load('fb_boodskap', TRUE);
                            $devicesArr = $ci->config->item('devicesArr', 'fb_boodskap');
                            foreach ($devicesArr as $value) {                          
                      ?>
                      <option value="<?= $value['value'] ?>"><?= $value['value'] ?></option>
                     <?php } ?>
                    </select>
          </div>
          </div>          
					<div class="form-group">
					<div class="form-group col-md-12">
					<label>Device Status:</label>
					<select name="device_status" id="devices_status" class="form-control">
                      <option selected value="">Choose...</option>
                      <option value="true">ON</option>
                      <option value="false">OFF</option>
                    </select>
				  </div>
					</div>
					
				</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Add Device</button>
					<button type="button" class="btn btn-secondary clear">Clear</button>
				</div>
				
			</div>
		</form>
	</div>
</div>

