<!-- header header  -->
<?php $this->load->view('include/header_view');	?>
<!-- End header header --> 
<!-- Left Sidebar  -->
<?php $this->load->view('include/left-sidebar');	?>
<!-- End Left Sidebar  --> 
<!-- Page wrapper  -->
<div class="page-wrapper"> 

	 <!-- Bread crumb -->
    <div class="row page-titles">
      <div class="col-md-5 align-self-center">
        <h3 class="text-primary">Manage Users</h3>
      </div>
      <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo site_url("/dashboard"); ?>">Home</a></li>
          <li class="breadcrumb-item active">Manage Users</li>
        </ol>
      </div>
    </div>
    <!-- End Bread crumb -->
	
	<!-- Container fluid  -->
	<div class="container-fluid">
		<!-- Start Page Content -->
		
		<?php if($this->session->flashdata("success_msg")): ?>
		  <div class="row">
			<div class="alert alert-success text-white col-12" role="alert">
			  <?php echo $this->session->flashdata("success_msg"); ?>
			</div>
		  </div>
		<?php endif; ?>
		
		<?php if($this->session->flashdata("error_msg")): ?>
		  <div class="row">
			<div class="alert alert-danger text-white col-12" role="alert">
			  <?php echo $this->session->flashdata("error_msg"); ?>
			</div>
		  </div>
		<?php endif; ?>
		
		<?php if($add_user=="true"): ?>
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body"> 
						<p class="text-right"><a href="<?php echo site_url("user/add_user"); ?>" class="btn btn-primary">Add user</a></p>
					</div>
				</div>
			</div>
		</div>
		<?php endif; ?>
		
		<!-- End PAge Content -->
		
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-body"> 
						 <table class="table table-hover">
							  <thead>
								<tr>
								  <th scope="col">#</th>
								  <th scope="col">First Name</th>
								  <th scope="col">Last Name</th>
								  <th scope="col">Mobile</th>
								  <th scope="col">Email</th>
								  <th scope="col">Role</th>
								  <th scope="col">Action</th>
								</tr>
							  </thead>
							  <tbody>
							  
						   <?php foreach($uresult as $k => $urow): 
						           $cp = $urow["custom_params"];
								   $acp = json_decode($cp, true);
							  ?>
						     <tr>
							   <th><?php echo $k+1; ?></th>
							   <td><?php echo ucfirst($acp["first_name"]); ?></td>
							   <td><?php echo ucfirst($acp["last_name"]); ?></td>
							   <td><?php echo $acp["mobile"]; ?></td>
							   <td><?php echo $urow["email"]; ?></td>
							   <td><?php echo ucfirst($urow["role"]); ?></td>
							   <td>
							   <a href="<?php echo site_url("user/edit_user?email=".$urow["email"]); ?>" title="Edit user"><i class="fa fa-edit"></i></a>
							   <a href="#" data-email="<?=$urow["email"] ?>" class="delete-user" 
							    title="Delete user"><i class="fa fa-trash"></i></a>
							   </td>
							 </tr>
						   <?php endforeach; ?>
						   
						  <?php if( empty($uresult) ): ?>
						     <tr>
							  <td colspan="7" class="text-center">Users not found</td>
							 </tr>
						   <?php endif; ?>
						   
							   </tbody>
						    </table>
					</div>
				</div>
			</div>
		</div>
		
		
	</div>

 <!-- End Page wrapper  --> 
</div>
<!-- End Wrapper -->

<div class="modal" tabindex="-1" role="dialog" aria-labelledby="useredeleteModalLabel" aria-hidden="true" id="user-delete-modal">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="useredeleteModalLabel">Are you sure to delete this user?</h4>
      </div>
      <div class="modal-footer">
        <a href="javascript:void(0);"  class="btn btn-primary" id="modal-btn-yes">Yes</a>
        <button type="button" class="btn btn-default" id="modal-btn-no">No</button>
      </div>
    </div>
  </div>
</div>
  
<?php $this->load->view('include/footer');	?>