<div class="left-sidebar">
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li class="nav-devider"></li>
                        <!--<li class="nav-label">Home</li>-->
				<?php if( has_accessable("dashboard") ): ?>
                        <li>
						<a class="left-menu" href="<?php echo site_url("dashboard"); ?>" aria-expanded="false"><i class="fa fa-tachometer-alt"></i><span class="hide-menu">Dashboard</span></a>
                        </li>
				<?php endif; ?>
						<?php if(has_accessable("list_meter")): ?>
                         <li> 
						  <a class="left-menu" href="<?php echo site_url("meters"); ?>" aria-expanded="false"><i class="fa fa-bolt"></i><span class="hide-menu">Manage Meters</span></a>
                         </li>
						<?php endif; ?>
                        <?php if( has_accessable("list_notes") ): ?>   
                        <li>
                        <a class="left-menu" href="<?php echo site_url("notes"); ?>" aria-expanded="false"><i class="fa fa-sticky-note"></i><span class="hide-menu">Notes/Reminder</span></a>
                        </li>
                        <?php endif; ?>
						<?php if( has_accessable("user_management") ): ?>
						<li>
						<a href="javascript:void(0);" class="has-arrow left-menu" aria-expanded="false">
						<i class="fa fa-user"></i>
						<span class="hide-menu">User Management</span>
						</a>
							 <ul aria-expanded="false" class="collapse">
								<li><a href="<?php echo site_url("user/manage_users"); ?>">Manage Users</a></li>
								<li><a href="<?php echo site_url("user/manage_permissions"); ?>">Manage Permissions</a></li>
							 </ul>
						</li>
						<?php endif; ?>
                        <?php if( has_accessable("settings") ): ?>
                         <li> <a class="left-menu" href="<?php echo site_url("settings"); ?>" aria-expanded="false"><i class="fa fa-cog"></i><span class="hide-menu">Settings</span></a>
                        </li>
                        <?php endif; ?>
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </div>