  <!-- Page wrapper  -->
  <div class="page-wrapper"> 
    <!-- Bread crumb -->
    <div class="row page-titles">
      <div class="col-md-5 align-self-center">
        <h3 class="text-primary">Settings</h3>
      </div>
      <div class="col-md-7 align-self-center">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo site_url("dashboard");?>">Home</a></li>
          <li class="breadcrumb-item active">Settings</li>
        </ol>
      </div>
    </div>
    <!-- End Bread crumb --> 
    <!-- Container fluid  -->
	<div class="container-fluid">
         <!-- Start Page Content -->
		<div class="row justify-content">			
			<div class="col-md-12">
			  <div class="card" id="settings-card">
				<div class="card-title">Alert Settings - EB </div>
				<div class="card-body">
				  <form name="alert" id="alert-settings-form" method="post" action="<?php echo base_url('settings/create'); ?>">
					<?php if($this->session->flashdata('c_success')) {
					?>
					<div class="sufee-alert alert with-close alert-success alert-dismissible fade show">                       
					<?php echo $this->session->flashdata('c_success');  ?>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
					</div> 
					<?php } ?>                 
					
					<?php if($this->session->flashdata('c_failed')) {
					?>
					<div class="sufee-alert alert with-close alert-danger alert-dismissible fade show" data-dismiss="alert">
					<?php echo $this->session->flashdata('c_failed');  ?>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
					</div> 
					<?php } ?>   
					
					<input type="hidden" name="ebalert_id" value="<?= $rkey;?>">
					<div class="table-responsive-sm">
						<table class="table table-bordered m-b-20">
							<thead>
								<tr>
									<td>Values</th>
									<td>Min</th>
									<td>Max</th>
								</tr>
							</thead>
							<tbody>
								
								<tr>
									<th scope="col">R Phase</th>
									<td><input type="text" class="form-control col-md-8" name="rspmin" id="rspmin" value="<?php 
									if(empty($source['rspmin']))
									echo "0";
									else
									echo $source['rspmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="rspmax" id="rspmax" value="<?php 
									if(empty($source['rspmax']))
									echo "0";
									else
									echo $source['rspmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">B Phase</th>
									<td><input type="text" class="form-control col-md-8" name="bspmin" id="bspmin" value="<?php 
									if(empty($source['bspmin']))
									echo "0";
									else
									echo $source['bspmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="bspmax" id="bspmax" value="<?php echo $source['bspmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Y Phase</th>
									<td><input type="text" class="form-control col-md-8" name="yspmin" id="yspmin" value="<?php 
									if(empty($source['yspmin']))
									echo "0";
									else
									echo $source['yspmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="yspmax" id="yspmax" value="<?php 
									if(empty($source['yspmax']))
									echo "0";
									else
									echo $source['yspmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Total Phase</th>
									<td><input type="text" class="form-control col-md-8" name="spmin" id="spmin" value="<?php 
									if(empty($source['spmin']))
									echo "0";
									else
									echo $source['spmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="spmax" id="spmax" value="<?php 
									if(empty($source['spmax']))
									echo "0";
									else
									echo $source['spmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">R Three Phase</th>
									<td><input type="text" class="form-control col-md-8" name="rtpmin" id="rtpmin" value="<?php 
									if(empty($source['rtpmin']))
									echo "0";
									else
									echo $source['rtpmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="rtpmax" id="rtpmax" value="<?php 
									if(empty($source['rtpmax']))
									echo "0";
									else
									echo $source['rtpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">B Three Phase</th>
									<td><input type="text" class="form-control col-md-8" name="btpmin" id="btpmin" value="<?php 
									if(empty($source['btpmin']))
									echo "0";
									else
									echo $source['btpmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="btpmax" id="btpmax" value="<?php 
									if(empty($source['btpmax']))
									echo "0";
									else
									echo $source['btpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Y Three Phase</th>
									<td><input type="text" class="form-control col-md-8" name="ytpmin" id="ytpmin" value="<?php 
									if(empty($source['ytpmin']))
									echo "0";
									else
									echo $source['ytpmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="ytpmax" id="ytpmax" value="<?php 
									if(empty($source['ytpmax']))
									echo "0";
									else
									echo $source['ytpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Total Three Phase</th>
									<td><input type="text" class="form-control col-md-8" name="tpmin" id="tpmin" value="<?php 
									if(empty($source['tpmin']))
									echo "0";
									else
									echo $source['tpmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="tpmax" id="tpmax" value="<?php 
									if(empty($source['tpmax']))
									echo "0";
									else
									echo $source['tpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">R Phase Current</th>
									<td><input type="text" class="form-control col-md-8" name="rcpmin" id="rcpmin" value="<?php 
									if(empty($source['rcpmin']))
									echo "0";
									else
									echo $source['rcpmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="rcpmax" id="rcpmax" value="<?php 
									if(empty($source['rcpmax']))
									echo "0";
									else
									echo $source['rcpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">B Phase Current</th>
									<td><input type="text" class="form-control col-md-8" name="bcpmin" id="bcpmin" value="<?php 
									if(empty($source['bcpmin']))
									echo "0";
									else
									echo $source['bcpmin'];
									?>"></td>
									<td><input type="text" class="form-control col-md-8" name="bcpmax" id="bcpmax" value="<?php 
									if(empty($source['bcpmax']))
									echo "0";
									else
									echo $source['bcpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Y Phase Current</th>
									<td><input type="text" class="form-control col-md-8" name="ycpmin" id="ycpmin" value="<?php 
									if(empty($source['ycpmin']))
									echo "0";
									else
									echo $source['ycpmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="ycpmax" id="ycpmax" value="<?php 
									if(empty($source['ycpmax']))
									echo "0";
									else
									echo $source['ycpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Total Current</th>
									<td><input type="text" class="form-control col-md-8" name="tcpmin" id="tcpmin" value="<?php 
									if(empty($source['tcpmin']))
									echo "0";
									else
									echo $source['tcpmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="tcpmax" id="tcpmax" value="<?php 
									if(empty($source['tcpmax']))
									echo "0";
									else
									echo $source['tcpmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">R Phase Power</th>
									<td><input type="text" class="form-control col-md-8" name="rppmin" id="rppmin" value="<?php 
									if(empty($source['rppmin']))
									echo "0";
									else
									echo $source['rppmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="rppmax" id="rppmax" value="<?php 
									if(empty($source['rppmax']))
									echo "0";
									else
									echo $source['rppmax'];?>"></td>
									
								</tr>
								<tr>
									<th scope="col">B Phase Power</th>
									<td><input type="text" class="form-control col-md-8" name="bppmin" id="bppmin" value="<?php 
									if(empty($source['bppmin']))
									echo "0";
									else
									echo $source['bppmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="bppmax" id="bppmax" value="<?php 
									if(empty($source['bppmax']))
									echo "0";
									else
									echo $source['bppmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Y Phase Power</th>
									<td><input type="text" class="form-control col-md-8" name="yppmin" id="yppmin" value="<?php 
									if(empty($source['yppmin']))
									echo "0";
									else
									echo $source['yppmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="yppmax" id="yppmax" value="<?php 
									if(empty($source['yppmax']))
									echo "0";
									else
									echo $source['yppmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Total Power</th>
									<td><input type="text" class="form-control col-md-8" name="tppmin" id="tppmin" value="<?php 
									if(empty($source['tppmin']))
									echo "0";
									else
									echo $source['tppmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="tppmax" id="tppmax" value="<?php 
									if(empty($source['tppmax']))
									echo "0";
									else
									echo $source['tppmax']; ?>"></td>
									
								</tr>
								<tr>
									<th scope="col">Frequency</th>
									<td><input type="text" class="form-control col-md-8" name="fremin" id="fremin" value="<?php 
									if(empty($source['fremin']))
									echo "0";
									else
									echo $source['fremin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="fremax" id="fremax" value="<?php 
									if(empty($source['fremax']))
									echo "0";
									else
									echo $source['fremax']; ?>"></td>
									
								</tr>
								<tr>
								
									<th scope="col">Power Factor</th>
									<td><input type="text" class="form-control col-md-8" name="pfmin" id="pfmin" value="<?php 
									if(empty($source['pfmin']))
									echo "0";
									else
									echo $source['pfmin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="pfmax" id="pfmax" value="<?php 
									if(empty($source['pfmax']))
									echo "0";
									else
									echo $source['pfmax']; ?>"></td>
								</tr>
								<tr>
								
									<th scope="col">KVA</th>
									<td><input type="text" class="form-control col-md-8" name="kvamin" id="kvamin" value="<?php 
									if(empty($source['kvamin']))
									echo "0";
									else
									echo $source['kvamin']; ?>"></td>
									<td><input type="text" class="form-control col-md-8" name="kvamax" id="kvamax" value="<?php 
									if(empty($source['kvamax']))
									echo "0";
									else
									echo $source['kvamax']; ?>"></td>
								</tr>								
							</tbody>
								
						</table>
						<input type="hidden" name="meter_id" value="<?php echo $this->uri->segment(3);?>">
						<button type="submit" class="btn btn-primary">Save</button>
						<button class="btn btn-secondary reset-btn">Reset</button>
				
					</div>        
				  </form>
				</div>
			  </div>
			</div>
	</div>
            
    <!-- End Page wrapper  --> 
</div>
<!-- End Wrapper -->